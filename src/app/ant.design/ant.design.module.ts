import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// materials
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';

// andt
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzBackTopModule } from 'ng-zorro-antd/back-top';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzListModule } from 'ng-zorro-antd/list';
import { MatRadioModule } from '@angular/material/radio';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatExpansionModule } from '@angular/material/expansion';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';

import { MatProgressBarModule } from '@angular/material/progress-bar';

const antdModules = [
  NzIconModule,
  NzTableModule,
  NzDrawerModule,
  NzSwitchModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatBadgeModule,
  MatRadioModule,
  NzBadgeModule,
  NzDropDownModule,
  NzModalModule,
  NzCardModule,
  NzBackTopModule,
  NzRadioModule,
  NzDividerModule,
  NzStepsModule,
  NzListModule,
  MatSelectModule,
  NzButtonModule,
  NzInputModule,
  NzSelectModule,
  NzUploadModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  NzMenuModule,
  NzSpinModule,
  NzTabsModule,
  MatListModule,
  NzCheckboxModule,
  MatCardModule,
  NzPopconfirmModule,
  NzPopoverModule,
  NzLayoutModule,
  MatBottomSheetModule,
  MatExpansionModule,
  MatProgressBarModule,
];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...antdModules],
  exports: [...antdModules],
})
export class AntDesignModule {}
