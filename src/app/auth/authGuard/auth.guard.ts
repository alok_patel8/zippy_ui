import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { CommonService } from 'src/app/core/common.service';
import { AuthService } from '../authService/auth.service';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private commonService: CommonService
  ) {}
  canActivate(): boolean {
    if (this.authService.isUserLoggedIn()) {
      return true;
    } else {
      this.commonService.userLoggedStatus.emit('logout');
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Unauthorized request',
        showConfirmButton: false,
        footer: '',
        // footer: 'Please login to visit this page',
      });
      this.router.navigate(['zippy']);
      return false;
    }
  }
}
