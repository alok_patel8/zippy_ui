import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from 'src/app/core/common.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private commonSer: CommonService) {}

  getToken(): any {
    return localStorage.getItem('_token');
  }

  getCurrentUserRole(): any {
    return localStorage.getItem('userRole');
  }

  getUserDetails(): any {
    return this.commonSer.decryptData(localStorage.getItem('_crd'));
  }

  isUserLoggedIn(): boolean {
    return localStorage.getItem('_token') ? true : false;
  }

  SignupRequest(formData: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/auth/signup`,
      formData
    );
  }

  loginService(formData: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/auth/login`,
      formData
    );
  }

  SendOtpForrecoverPassword(email: any): Observable<any> {
    return this.http.get(`${environment.devenvironmentUrl}/auth/sendOTP`, {
      params: { email },
    });
  }
  ChangePasswordReq(payload: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/auth/changepassword`,
      payload
    );
  }
}
