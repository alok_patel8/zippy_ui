import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CommonService } from 'src/app/core/common.service';
import { Router } from '@angular/router';
import { AuthService } from '../authService/auth.service';
import Swal from 'sweetalert2';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private injector: Injector,
    private message: CommonService,
    private router: Router,
    private authService: AuthService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    const authService = this.injector.get(AuthService);
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authService.getToken()}`,
      },
    });
    return next.handle(req).pipe(
      catchError((error): any => {
        if (error.status === 401 || error.status === 403) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: error.error,
            showConfirmButton: false,
            footer: '',
            // footer: 'Please login to visit this page',
          });
          this.message.userLoggedStatus.emit('logout');
          localStorage.removeItem('_token');
          localStorage.removeItem('_crd');
          this.router.navigate(['/zippy/user']);
        }
        return throwError(error);
      })
    );
  }
}
