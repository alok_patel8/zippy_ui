import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from '../sign-in/sign-in.component';

const routes: Routes = [
  {
    pathMatch: 'full',
    path: 'signIn',
    component: SignInComponent,
  },
  {
    path: '',
    redirectTo: 'signIn',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
