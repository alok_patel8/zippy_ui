import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/core/common.service';
import { AuthService } from '../authService/auth.service';

@Component({
  selector: 'zip-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.less'],
})
export class ForgetPasswordComponent implements OnInit {
  loading = false;
  isOTPSend = false;
  isPasswordType = false;
  userRole: any;
  formdata = this.fb.group({
    email: ['satyam@gmail.com', Validators.email],
    password: ['password', [Validators.required, Validators.minLength(8)]],
    code: [
      '121212',
      [Validators.required, Validators.minLength(6), Validators.maxLength(6)],
      ,
    ],
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private commonServic: CommonService,
    private spinner: NgxSpinnerService,
    private drawerRef: NzDrawerRef
  ) {}

  get Forgeterr(): any {
    return this.formdata.controls;
  }
  ngOnInit(): void {}

  sendOTP(email: string): void {
    if (this.formdata.controls.email.valid) {
      this.spinner.show();
      this.authService.SendOtpForrecoverPassword(email).subscribe(
        (res: any) => {
          this.spinner.hide();
          this.userRole = res?.userRole;
          this.commonServic.showSnackBar(res?.message);
          this.isOTPSend = !this.isOTPSend;
        },
        (err: any) => {
          this.spinner.hide();
          this.commonServic.showSnackBar(err?.error?.message);
        }
      );
    }
  }

  onSubmit(): void {
    const entity = { payload: this.formdata.value, userRole: this.userRole };
    if (this.formdata.valid) {
      this.spinner.show();
      this.authService.ChangePasswordReq(entity).subscribe(
        (res: any) => {
          this.spinner.hide();
          this.commonServic.showSnackBar(res?.message);
          this.drawerRef.close();
          this.isOTPSend = !this.isOTPSend;
        },
        (err: any) => {
          this.spinner.hide();
          this.commonServic.showSnackBar(err?.error?.message);
        }
      );
    }
  }
  editeMail(): void {
    this.isOTPSend = !this.isOTPSend;
  }
}
