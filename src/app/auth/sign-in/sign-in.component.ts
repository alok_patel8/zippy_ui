import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroupDirective, Validators } from '@angular/forms';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';
import { AuthService } from '../authService/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/core/common.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ForgetPasswordComponent } from '../forget-password/forget-password.component';
import { CartService } from 'src/app/core/cart.service';

declare let gapi: any;

@Component({
  selector: 'zip-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less'],
})
export class SignInComponent implements OnInit {
  public auth2: any;
  loginForm = this.fb.group({
    email: ['', Validators.email],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });
  signupForm = this.fb.group({
    username: ['', Validators.required],
    referralCode: [''],
    email: ['', Validators.email],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(8),
        // Validators.pattern(
        //   '/^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,}$/'
        // ),
      ],
    ],
  });

  isOTPSend = false;
  loading = false;
  isloginFormOpen: boolean;
  haveRfCode = false;
  isPasswordType = false;

  @Input() formType: boolean;
  constructor(
    private fb: FormBuilder,
    private drawerRef: NzDrawerRef,
    private AuthService: AuthService,
    private spinner: NgxSpinnerService,
    private commonService: CommonService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nzDrawar: NzDrawerService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.isloginFormOpen = this.formType;
  }

  // ngOnChanges(): void {
  //   this.isloginFormOpen = this.formType;
  //   console.log('jsjhsdsd', this.isloginFormOpen);
  // }
  get Signuperr(): any {
    return this.signupForm.controls;
  }
  get loginerr(): any {
    return this.loginForm.controls;
  }

  // tslint:disable-next-line: typedef
  public googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id:
          '457900117685-t11a84comeoku5gsb7n69g30lcvpj8k1.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email',
      });
      this.attachSignin(document.getElementById('googleBtn'));
      // this.isloginFormOpen
      //   ? this.attachSignin(document.getElementById('googleBtn'))
      //   : this.attachSignin(document.getElementById('googleSignupBtn'));
      // this.attachSignin(document.getElementById('googleSignupBtn'));
    });
  }
  // tslint:disable-next-line: typedef
  public attachSignin(element) {
    this.auth2.attachClickHandler(
      element,
      {},
      (googleUser) => {
        const profile = googleUser.getBasicProfile();
        console.log('Token || ' + googleUser.getAuthResponse().id_token);
        console.log('ID: ' + profile.getId());
        console.log('Name: ' + profile.getName());
        console.log('Image URL: ' + profile.getImageUrl());
        console.log('Email: ' + profile.getEmail());
        //YOUR CODE HERE
      },
      (error) => {
        alert(JSON.stringify(error, undefined, 2));
      }
    );
  }

  ngAfterViewInit() {
    this.googleInit();
  }

  // login
  onLogin(): void {
    if (this.loginForm.status === 'VALID') {
      this.spinner.show();
      this.AuthService.loginService(this.loginForm.value).subscribe(
        (res: any) => {
          console.log('onLogin', res);
          if (res?.user?.role === 'seller') {
            this.router.navigate(['/zippy/seller']);
          } else if (res?.user?.role === 'admin') {
            this.router.navigate(['/zippy/admin']);
          }
          localStorage.setItem(
            '_crd',
            this.commonService.encryptData(res?.user)
          );
          // console.log(
          //   this.commonService.decryptData(
          //     this.commonService.encryptData(res?.user)
          //   )
          // );
          localStorage.setItem('_token', res?.token);
          this.commonService.userLoggedStatus.emit(res);
          this.spinner.hide();
          this.closeDrawer();
        },
        (err: any) => {
          this.spinner.hide();
          this.commonService.showSnackBar(err.error.message);
        }
      );
    }
  }

  // signup
  onSubmit(formDirective: FormGroupDirective): void {
    if (this.signupForm.status === 'VALID') {
      this.spinner.show();
      // this.loading = true;
      this.AuthService.SignupRequest(this.signupForm.value).subscribe(
        (res: any) => {
          this.spinner.hide();
          // this.loading = false;
          this.commonService.userLoggedStatus.emit(res);
          localStorage.setItem('token', res);
          formDirective.resetForm();
          this.signupForm.reset();
          this.closeDrawer;
          this.commonService.showSnackBar(res.message);
        },
        (err: any) => {
          this.spinner.hide();
          // this.loading = false;

          this.commonService.showSnackBar(err.error.message);
          console.log('err', err);
        }
      );
    }
  }

  ForgotPassword(): void {
    // this.isOTPSend = false;
    this.nzDrawar.create({
      nzContent: ForgetPasswordComponent,
      nzTitle: null,
      nzFooter: null,
      nzWidth: 650,
      nzPlacement: 'left',
      nzMaskClosable: false,
    });
  }

  changeForm(): void {
    this.isloginFormOpen = !this.isloginFormOpen;
  }

  haveReferralCode(): void {
    this.haveRfCode = true;
  }

  closeDrawer(): void {
    this.drawerRef.close();
  }
}
