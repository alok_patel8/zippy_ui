import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'src/app/core/admin.service';
import { Location } from '@angular/common';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { SellerComponent } from '../../seller.component';
import { SellerService } from 'src/app/core/seller.service';
import { CommonService } from 'src/app/core/common.service';
import { NgxSpinnerService } from 'ngx-spinner';

function getBase64(file: File): Promise<string | ArrayBuffer | null> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

@Component({
  selector: 'zip-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.less'],
})
export class AddProductComponent implements OnInit {
  category: any;
  current = 0;
  previewImage: string | undefined = '';
  previewVisible = false;
  images: any;
  sellingPrice: number;
  discount: number;
  numberRegx = new RegExp('^[0-9]*$');
  zippyImage = [];
  selectImage: any;
  itemId = null;
  tempArray = [];
  shopId: any | undefined = '';

  fileList: NzUploadFile[] = [];
  duration = [
    {
      days: 1,
      discription: 'days',
    },
    {
      days: 7,
      discription: 'weeks',
    },
    {
      days: 30,
      discription: 'months',
    },
    {
      days: 365,
      discription: 'years',
    },
  ];

  availableIn = [
    '100 gm',
    '150 gm',
    '200 gm',
    '250 gm',
    '400 gm',
    '500 gm',
    '1 kg',
    '5 kg',
    '10 kg',
    '20 kg',
    '50 kg',
  ];

  categoryForm = this.fb.group({
    branchId: '',
    productName: ['', Validators.required],
    productprice: [
      ,
      [Validators.required, Validators.pattern('^[0-9]*$'), Validators.min(1)],
    ],
    categoryName: ['', Validators.required],
    brandName: ['', Validators.required],
    availablein: ['', Validators.required],
    inStockNumber: [
      0,
      [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.max(500),
      ],
    ],
    expirydate: [''],
    keyFeatures: [''],
    ingredients: [''],
    duration: [''],
    discount: [, [Validators.pattern('^[0-9]*$'), Validators.max(100)]],
  });

  get geterr(): any {
    return this.categoryForm.controls;
  }

  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = this.fileList.concat(file);
    return false;
  };

  constructor(
    private adminService: AdminService,
    private sellerService: SellerService,
    private fb: FormBuilder,
    private router: Router,
    private location: Location,
    private commonService: CommonService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      console.log('this is act', res);
      this.itemId = res?.id;
    });
  }

  ngOnInit(): void {
    this.spinner.show();
    this.commonService.ItemAddEvent.subscribe((res: Response) => {
      this.initPageAfterAddItem();
    });
    this.adminService.getCategory().subscribe((res: any) => {
      this.spinner.hide();
      this.category = res?.data;
    });
    this.getProductById();
    this.getShopId();
  }

  getShopId(): void {
    let user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.shopId = user?._id;
    this.categoryForm.controls.branchId.setValue(this.shopId);
  }

  initPageAfterAddItem(): void {
    console.log('run');

    this.current = 0;
    this.images = [];
  }
  back(): void {
    if (this.current === 1) {
      this.current = 0;
    } else if (this.current === 0) {
      this.location.back();
    }
  }
  next(formDirective: FormGroupDirective): void {
    if (this.current === 0) {
      this.current = 1;
      this.spinner.show();
      this.sellerService.GetZippyImage().subscribe((image: any) => {
        this.zippyImage = image?.data;
        this.spinner.hide();
      });
      this.spinner.hide();
    } else if (this.current === 1) {
      this.spinner.show();
      const formdata = new FormData();
      this.images?.forEach((ele: any) => {
        formdata.append('myImages', ele.originFileObj);
      });
      Object.keys(this.categoryForm.value).forEach((key, index) => {
        formdata.append(key, this.categoryForm.value[key]);
      });
      formdata.append('imagelinks', this.selectImage);
      this.sellerService.addProduct(formdata).subscribe((res: any) => {
        this.spinner.hide();
        this.commonService.showSnackBar(res.message);
        this.commonService.ItemAddEvent.emit('item');
        formDirective.resetForm();
        this.categoryForm.reset();
        this.discount = 0;
      });
    }
  }

  change(event) {
    if (event.isUserInput) {
      switch (event.source.selected) {
        case true:
          this.tempArray.push(event.source.value);
          break;
        case false:
          this.tempArray.splice(this.tempArray.indexOf(event.source.value), 1);
          break;
        default:
          break;
      }
    }
    this.categoryForm.controls.availablein.setValue(this.tempArray);
  }
  selectImageChange(event: any): void {
    if (event.type === 'success' || event.type === 'progress') {
      this.images = event?.fileList;
      console.log('onUploadClicked($event)', this.images);
    }
  }

  onSelectImages(event: any): void {
    this.selectImage = event;
  }

  calculateDiscount(): void {
    const discountPercent = this.categoryForm.value.discount;
    const price = this.categoryForm.value.productprice;
    if (
      discountPercent !== null &&
      price !== null &&
      this.numberRegx.test(discountPercent) &&
      this.numberRegx.test(price)
    ) {
      this.discount = Number((discountPercent / 100) * price);
      this.sellingPrice = Number(price - this.discount);
    }
  }

  getProductById(): void {
    if (this.itemId) {
      console.log('update res', this.itemId);
      this.sellerService.getProductById(this.itemId).subscribe((res: any) => {
        console.log('update res', res);
      });
    }
  }

  handlePreview = async (file: NzUploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj!);
    }
    this.previewImage = file.url || file.preview;
    this.previewVisible = true;
  };
}
