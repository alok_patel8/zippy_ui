import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { ActivatedRoute, Router } from '@angular/router';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from 'src/app/core/admin.service';
import { CommonService } from 'src/app/core/common.service';
import { SellerService } from 'src/app/core/seller.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'zip-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.less'],
})
export class ProductViewComponent implements OnInit {
  SERVER_URL = environment.devenvironmentUrl;
  user = this.commonService.decryptData(localStorage.getItem('_crd'));
  branchId = this.user?._id;
  category: any;
  productList: Array<any>[] = [];
  loader = false;
  loaderwarn = false;
  loadergreen = false;

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(
    private sellerServices: SellerService,
    private spinner: NgxSpinnerService,
    private adminService: AdminService,
    private modal: NzModalService,
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private draweService: NzDrawerService
  ) {
    this.commonService.deleteEvent.subscribe((res: Response) => {
      // this.getTabaleData();
    });
  }

  ngOnInit(): void {
    // this.adminService.getCategory().subscribe((res: any) => {
    //   this.category = res.data;
    //   console.log('category', this.category);
    // });
    this.getShopData();
  }

  getShopData(): void {
    this.loader = true;
    this.commonService
      .getSellerShopDataById(this.branchId)
      .subscribe((res: any) => {
        this.loader = false;
        this.productList = res?.data;
        this.ModifyProductList();
      });
  }

  ModifyProductList(): void {
    let array = [];
    let tempArray = [];
    this.productList.forEach((ele: any) => {
      array = [];
      tempArray = [];
      ele?.quantitySet?.forEach((i: any) => {
        if (tempArray.length < 3) {
          tempArray.push(i);
        } else {
          array.push(tempArray);
          tempArray = [];
          tempArray.push(i);
        }
      });
      array.push(tempArray);
      ele.tempArray = array;
      this.productList = this.productList;
    });
  }

  openFilter(): void {}

  calculateDiscount(discountPercent: any, price: any): Number {
    const discount = Number((discountPercent / 100) * price);
    return Math.ceil(price - discount);
  }

  onDeleteProduct(id: any): void {
    this.modal.confirm({
      nzTitle: 'Are you sure delete this item?',
      nzContent: 'This action is not reversable',
      nzOkText: 'Yes, Delete',
      nzOkDanger: true,
      nzOkType: 'danger',
      nzOnOk: () => console.log('OK'),
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancel'),
    });
  }

  onStatusChange(event: any, id: any, status: any): void {
    console.log(event, id, status);
    if (event == 'stop') {
      this.loaderwarn = true;
    } else {
      this.loadergreen = true;
    }
    this.modal.confirm({
      nzTitle:
        event == 'stop'
          ? 'Are you really want to remove this product from market.'
          : 'Are you really want to start selling these product in market.',
      nzOkText: event == 'stop' ? 'Remove' : 'Yes',
      nzOnOk: () => {
        this.loaderwarn = false;
        this.loadergreen = false;
      },
      nzOnCancel: () => {
        this.loaderwarn = false;
        this.loadergreen = false;
      },
    });
  }

  getStatus(status: any): boolean {
    return status === 'active' ? true : false;
  }
}
