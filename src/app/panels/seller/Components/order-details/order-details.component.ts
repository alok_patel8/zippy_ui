import { Component, OnInit } from '@angular/core';
import { CustomActionsComponent } from 'src/app/component/custom-actions/custom-actions.component';
import { CustomColumnComponent } from 'src/app/component/dynamic-table/custom-column/custom-column.component';

@Component({
  selector: 'zip-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.less'],
})
export class OrderDetailsComponent implements OnInit {
  dataSource: any;
  radioValue: any = '-1';
  settings: any = {
    hideSubHeader: true,
    actions: {
      position: 'right',
      add: false,
      edit: false,
      delete: false,
      custom: [],
    },
    pager: {
      display: false,
    },
    columns: {
      // checkBox: {
      //   type: 'custom',
      //   filter: false,
      //   width: '10px',
      //   renderComponent: CustomColumnComponent,
      // },
      id: {
        title: 'Order id',
        sort: false,
      },
      customer: {
        title: 'Customer',
        sort: false,
      },
      items: {
        title: "Order Item's",
        sort: false,
      },
      orderDate: {
        title: 'Order Date',
        sort: false,
      },
      price: {
        title: 'Amount',
        sort: false,
      },
      status: {
        title: 'Status',
        sort: false,
      },
      payment: {
        title: 'Payment',
        sort: false,
      },
    },
  };

  constructor() {}

  ngOnInit(): void {
    this.dataSource = [
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
      {
        id: 194394737797,
        customer: 'Leanne Graham',
        items: 'Bret',
        orderDate: 'Sincere@april.biz',
        price: 299,
        status: 'pending',
        payment: 'UPI',
      },
    ];
    this.setCustomActionTable();
  }

  setCustomActionTable(): void {
    this.settings = Object.assign(
      {},
      {
        noDataMessage: 'No Data Found',
        mode: 'external',
        hideSubHeader: true,
        actions: false,
        columns: this.settings.columns,
        pager: {
          display: false,
        },
      }
    );
    this.settings.columns.CustomActions = {
      title: 'Actions',
      type: 'custom',
      filter: false,
      valuePrepareFunction: (value: any, row: any, cell: any) => {
        return { value, row, cell };
      },
      renderComponent: CustomActionsComponent,
      onComponentInitFunction(instance: any): void {},
    };
  }
}
