import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'zip-received-orders',
  templateUrl: './received-orders.component.html',
  styleUrls: ['./received-orders.component.less'],
})
export class ReceivedOrdersComponent implements OnInit {
  loader = false;
  loading = false;
  source: any;
  currentStepIndex = 0;
  selectedProductId: any;

  // listOfData = [
  //   {
  //     key: '1',
  //     name: 'John Brown',
  //     age: 32,
  //     address: 'New York No. 1 Lake Park',
  //   },
  //   {
  //     key: '2',
  //     name: 'Jim Green',
  //     age: 42,
  //     address: 'London No. 1 Lake Park',
  //   },
  //   {
  //     key: '3',
  //     name: 'Joe Black',
  //     age: 32,
  //     address: 'Sidney No. 1 Lake Park',
  //   },
  // ];

  constructor(
    private commonService: CommonService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe((params: any) => {
      this.selectedProductId = params?.orderId;
    });
  }

  ngOnInit(): void {
    this.getStepsData();
  }

  prev(): void {
    this.currentStepIndex -= 1;
  }
  next(): void {
    this.currentStepIndex += 1;
  }
  open(index: number): void {
    this.currentStepIndex = index;
  }

  getStepsData(): void {
    this.loading = true;
    this.commonService.getOrders('_id', this.selectedProductId).subscribe(
      (data: any) => {
        this.loading = false;
        this.source = data?.data[0];
        this.modifyData();
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  modifyData(): void {
    this.loading = true;
    const arr = [];
    let obj: any = {};
    obj = {};
    this.source?.orderlist?.forEach((i: any) => {
      obj = {};
      obj.discount = i?.l?.q?.discount;
      obj.orderQuantity = i?.l?.orderQuantity;
      obj.packof = i?.l?.q?.availablein;
      obj.productName = i?.productName;
      obj.productPrice = i?.l?.q?.productprice;
      obj.item = i?.l?.q?.productId;
      arr.push(obj);
      if (arr.length === this.source?.orderlist?.length) {
        this.loading = false;
        this.source.orderlist = arr;
      }
    });
  }

  calculateSellingPrice(discountPercent: any, price: any): number {
    const discount = Number((discountPercent / 100) * price);
    return Math.ceil(price - discount);
  }
  openFilter(): void {}
}
