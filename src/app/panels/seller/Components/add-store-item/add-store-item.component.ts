import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'src/app/core/admin.service';
import { SellerService } from 'src/app/core/seller.service';
import { CommonService } from 'src/app/core/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { v4 as uuidv4 } from 'uuid';
import { Location } from '@angular/common';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'zip-add-store-item',
  templateUrl: './add-store-item.component.html',
  styleUrls: ['./add-store-item.component.less'],
})
export class AddStoreItemComponent implements OnInit {
  SERVER_URL = environment.devenvironmentUrl;
  fileList: NzUploadFile[] = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  uploading = false;
  currentStepCount = 0;
  myForm: any;
  numberRegx = new RegExp('^[0-9]*$');
  itemId: any;
  category: any;
  shopId: any;
  tempArray: any;
  zippyImage: any;
  productId: any;
  selectImage: any;
  disabled = false;
  isSelectableImageList = false;

  constructor(
    private adminService: AdminService,
    private sellerService: SellerService,
    private fb: FormBuilder,
    private router: Router,
    // private location: Location,
    private commonService: CommonService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private loaction: Location
  ) {
    this.activatedRoute.paramMap.subscribe((res) => {
      this.itemId = res;
      this.itemId = this.itemId?.params?.id;
    });
  }

  availableIn = [
    '100 gm',
    '150 gm',
    '200 gm',
    '250 gm',
    '400 gm',
    '500 gm',
    '1 kg',
    '5 kg',
    '10 kg',
    '20 kg',
    '50 kg',
  ];

  storeItem = this.fb.group({
    shopid: '',
    productName: ['', Validators.required],
    categoryName: ['', Validators.required],
    brandName: ['', Validators.required],
    keyFeatures: [''],
    ingredients: [''],
    quantity: this.fb.array([this.quntityForm()]),
  });

  quntityForm(): any {
    return this.fb.group({
      productid: '',
      productprice: [
        ,
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.min(1),
        ],
      ],
      availablein: [, Validators.required],
      inStockNumber: [
        0,
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.max(500),
        ],
      ],
      discount: [, [Validators.pattern('^[0-9]*$'), Validators.max(100)]],
      discountPrice: [0],
      sellingPrice: [0],
    });
  }

  get quantity(): any {
    return this.storeItem.get('quantity') as FormArray;
  }
  get geterr(): any {
    return this.storeItem.controls;
  }

  getValidity(i): any {
    return (this.storeItem.get('quantity') as FormArray).controls[i];
  }

  addQuntity(): any {
    this.quantity.push(this.quntityForm());
  }

  removeQuntityForm(i: number): any {
    this.quantity.removeAt(i);
  }

  ngOnInit(): void {
    this.SERVER_URL = `${this.SERVER_URL}/seller/uploadimage?id=${this.productId}`;
    this.spinner.show();
    this.commonService.ItemAddEvent.subscribe((res: Response) => {
      this.currentStepCount = 0;
    });
    this.adminService.getCategory().subscribe((res: any) => {
      this.spinner.hide();
      this.category = res?.data;
    });
    this.sellerService.getProductImage().subscribe((res: any) => {
      console.log('this is iamge', res);
      this.zippyImage = res?.data;
    });

    if (this.itemId) {
      this.getProductById();
    }
    this.getShopId();
    this.myForm = this.storeItem.get('quantity') as FormArray;
  }

  back(): void {
    if (this.currentStepCount === 0) {
      // this.router.navigate(['/zippy/seller/products']);
      this.loaction.back();
    }
    this.currentStepCount > 0
      ? (this.currentStepCount -= 1)
      : (this.currentStepCount = 0);
  }
  next(formDirective: FormGroupDirective): void {
    if (this.currentStepCount === 0) {
      this.saveFirstStep(formDirective);
    } else if (this.currentStepCount === 1) {
      this.saveSecondStep(formDirective);
    } else if (this.currentStepCount === 2) {
      this.saveLastStep(formDirective);
    }
  }

  saveFirstStep(formDirective: FormGroupDirective): void {
    if (!this.checkInputFieldsValid()) {
      this.spinner.show();
      this.sellerService.addFirstStepData(this.storeItem.value).subscribe(
        (res: any) => {
          this.spinner.hide();
          const r = this.storeItem.get('quantity') as FormArray;
          // this.commonService.ItemAddEvent.emit('item');
          this.commonService.showSnackBar(res?.message);
          console.log('this is reeeeljsa', res);

          this.productId = res?.saved?._id;
          this.currentStepCount < 2
            ? (this.currentStepCount += 1)
            : (this.currentStepCount = 2);
        },
        (err: any) => {
          this.spinner.hide();
          this.commonService.showSnackBar(err?.error?.message);
        }
      );
    }
  }

  saveSecondStep(formDirective: FormGroupDirective): void {
    const r = this.storeItem.get('quantity') as FormArray;
    if (!this.isValidFormArray()) {
      this.spinner.show();

      this.sellerService.addProductQuant(r?.value).subscribe(
        (res: any) => {
          this.spinner.hide();
          this.commonService.showSnackBar(res?.message);
          this.currentStepCount < 2
            ? (this.currentStepCount += 1)
            : (this.currentStepCount = 2);
        },
        (err: any) => {
          this.spinner.hide();
          this.commonService.showSnackBar(err?.error?.message);
        }
      );
    }
  }

  saveLastStep(formDirective: FormGroupDirective): void {
    this.spinner.show();
    const formData = new FormData();
    this.fileList.forEach((file: any) => {
      formData.append('files', file);
    });
    this.sellerService.uploadProductImage(this.productId, formData).subscribe(
      (res: any) => {
        this.spinner.hide();
        this.commonService.showSnackBar(res?.message);
        this.loaction.back();
      },
      (err: any) => {
        this.spinner.hide();
        this.commonService.showSnackBar(err?.error?.message);
      }
    );
  }

  onSelectImages(event: any): void {
    this.selectImage = event;
    if (this.selectImage?.length > 0) {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
  }
  beforeUpload = (file: NzUploadFile): boolean => {
    this.fileList = this.fileList.concat(file);
    if (this.fileList?.length > 0) {
      this.isSelectableImageList = true;
    } else {
      this.isSelectableImageList = false;
    }
    return false;
  };

  isValidFormArray(): boolean {
    const r = this.storeItem.get('quantity') as FormArray;
    return r.status === 'VALID' ? false : true;
  }
  checkInputFieldsValid(): boolean {
    return !(
      this.storeItem.controls.productName.valid &&
      this.storeItem.controls.categoryName.valid &&
      this.storeItem.controls.brandName.valid
    );
  }

  getProductById(): void {
    if (this.itemId) {
      this.sellerService.getProductById(this.itemId).subscribe((res: any) => {
        const productDetails = res?.data[0];
        const qV = res?.quantitySet;
        const formControl = this.storeItem.controls;
        formControl.brandName.setValue(productDetails?.brandName);
        formControl.categoryName.setValue(productDetails?.categoryName);
        formControl.productName.setValue(productDetails?.productName);
        formControl.ingredients.setValue(productDetails?.ingredients);
        formControl.keyFeatures.setValue(productDetails?.keyFeatures);
        formControl.keyFeatures.setValue(productDetails?.keyFeatures);
        const qForm = this.storeItem.get('quantity') as FormArray;
        // const qV = productDetails.quantitySet;
        const quLength = qV.length;
        let count = 0;
        qV.forEach((ele: any) => {
          if (quLength > count) {
            if (count !== 0) {
              this.addQuntity();
            }
            qForm.at(count).patchValue({
              _id: ele?._id,
              productprice: ele?.productprice,
              availablein: ele?.availablein,
              inStockNumber: ele?.inStockNumber,
              discount: ele?.discount,
              discountPrice: Math.ceil(
                (ele?.discount / 100) * ele?.productprice
              ),
              sellingPrice: Math.ceil(
                ele?.productprice -
                  Math.ceil((ele?.discount / 100) * ele?.productprice)
              ),
            });

            count += 1;
          }
        });
        this.zippyImage = productDetails?.imageUrl;
      });
    }
  }
  getShopId(): void {
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.shopId = user?._id;
    this.storeItem.controls.shopid.setValue(this.shopId);
  }

  // change(event): void {
  //   if (event.isUserInput) {
  //     switch (event.source.selected) {
  //       case true:
  //         this.tempArray.push(event.source.value);
  //         break;
  //       case false:
  //         this.tempArray.splice(this.tempArray.indexOf(event.source.value), 1);
  //         break;
  //       default:
  //         break;
  //     }
  //   }
  //   this.storeItem.controls.availablein.setValue(this.tempArray);
  // }

  calculateDiscount(i): void {
    this.myForm = (this.storeItem.get('quantity') as FormArray).at(i);
    const discountPercent = this.myForm?.value.discount;
    const price = this.myForm?.value.productprice;
    this.myForm.patchValue({
      productid: this.productId,
    });
    if (
      discountPercent !== null &&
      price !== null &&
      this.numberRegx.test(discountPercent) &&
      this.numberRegx.test(price)
    ) {
      const discountPrice = Math.ceil((discountPercent / 100) * price);
      const sellingPrice = price - discountPrice;

      this.myForm.patchValue({
        productid: this.productId,
        discountPrice,
        sellingPrice,
      });
    }
  }
}
