import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/authGuard/auth.guard';
import { DynamicTableComponent } from 'src/app/component/dynamic-table/dynamic-table.component';
import { AddProductComponent } from './Components/add-product/add-product.component';
import { AddStoreItemComponent } from './Components/add-store-item/add-store-item.component';
import { OrderDetailsComponent } from './Components/order-details/order-details.component';
import { ProductViewComponent } from './Components/product-view/product-view.component';
import { ReceivedOrdersComponent } from './Components/received-orders/received-orders.component';
import { SellerDashboardComponent } from './seller-dashboard/seller-dashboard.component';
import { SellerComponent } from './seller.component';

const routes: Routes = [
  {
    // pathMatch: 'full',
    component: SellerComponent,
    path: 'dashboard',
    children: [
      {
        path: '',
        component: SellerDashboardComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard],
      },
      {
        path: 'addproduct',
        component: AddStoreItemComponent,
        pathMatch: 'full',
      },
      {
        path: 'edit/:id',
        component: AddStoreItemComponent,
        pathMatch: 'full',
      },
      {
        path: 'products',
        component: ProductViewComponent,
        pathMatch: 'full',
      },
      {
        path: 'class/:className',
        component: OrderDetailsComponent,
        pathMatch: 'full',
      },
      {
        path: 'order-details/:orderId',
        component: ReceivedOrdersComponent,
        pathMatch: 'full',
      },
    ],
  },
  // {
  //   path: '',
  //   component: SellerDashboardComponent,
  //   pathMatch: 'full',
  //   canActivate: [AuthGuard],
  // },
  // {
  //   path: 'addproduct',
  //   component: AddStoreItemComponent,
  //   // component: AddProductComponent,
  //   pathMatch: 'full',
  // },
  // {
  //   path: 'products',
  //   component: DynamicTableComponent,
  //   pathMatch: 'full',
  // },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellerRoutingModule {}
