import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/core/common.service';
import { SellerService } from 'src/app/core/seller.service';

@Component({
  selector: 'zip-seller',
  templateUrl: './seller.component.html',
  styleUrls: ['./seller.component.less'],
})
export class SellerComponent implements OnInit {
  userRole: any;
  constructor(
    private sellerService: SellerService,
    private commonService: CommonService
  ) {}

  ngOnInit(): void {
    window.scroll(0, 0);
    // const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    // this.userRole = user?.role;
    document.getElementById('footer').style.display = 'none';
    this.sellerService.SellerDashboard().subscribe((res: any) => {});
  }

  // toggle(event: any): void {
  //   const ele = document.getElementById('main');
  //   if (event) {
  //     ele.style.marginLeft = '79px';
  //     ele.style.transition = '.2s';
  //   } else {
  //     ele.style.marginLeft = '200px';
  //   }
  // }
}
