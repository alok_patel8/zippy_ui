import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SellerRoutingModule } from './seller-routing.module';
import { SellerDashboardComponent } from './seller-dashboard/seller-dashboard.component';
import { AntDesignModule } from 'src/app/ant.design/ant.design.module';
import { LayoutsModule } from 'src/app/layouts/layouts/layouts.module';
import { SellerComponent } from './seller.component';
import { AddProductComponent } from './Components/add-product/add-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AddStoreItemComponent } from './Components/add-store-item/add-store-item.component';
import { ProductViewComponent } from './Components/product-view/product-view.component';
import { ReceivedOrdersComponent } from './Components/received-orders/received-orders.component';
import { OrderDetailsComponent } from './Components/order-details/order-details.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
// import { MatFileUploadModule } from 'mat-file-upload'';

@NgModule({
  declarations: [
    SellerDashboardComponent,
    SellerComponent,
    AddProductComponent,
    AddStoreItemComponent,
    ProductViewComponent,
    ReceivedOrdersComponent,
    OrderDetailsComponent,
  ],
  imports: [
    CommonModule,
    SellerRoutingModule,
    ReactiveFormsModule,
    LayoutsModule,
    FormsModule,
    Ng2SmartTableModule,
    AntDesignModule,
    NgxSpinnerModule,
    // MatFileUploadModule,
  ],
})
export class SellerModule {}
