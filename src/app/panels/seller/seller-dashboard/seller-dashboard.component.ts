import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'zip-seller-dashboard',
  templateUrl: './seller-dashboard.component.html',
  styleUrls: ['./seller-dashboard.component.less'],
})
export class SellerDashboardComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    window.scroll(0, 0);
  }
}
