import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AntDesignModule } from 'src/app/ant.design/ant.design.module';
import { AdminComponent } from './admin.component';
import { SidebarComponent } from 'src/app/layouts/sidebar/sidebar.component';
import { AppModule } from 'src/app/app.module';
import { LayoutsModule } from 'src/app/layouts/layouts/layouts.module';
import { AddCategoryComponent } from './Components/add-category/add-category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [AdminDashboardComponent, AdminComponent, AddCategoryComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutsModule,
    AntDesignModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  // exports: [SidebarComponent],
})
export class AdminModule {}
