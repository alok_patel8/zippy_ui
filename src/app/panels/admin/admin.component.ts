import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/core/admin.service';

@Component({
  selector: 'zip-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less'],
})
export class AdminComponent implements OnInit {
  constructor(private adminService: AdminService) {}

  ngOnInit(): void {
    this.adminService.AdminDashboard().subscribe((res: any) => {});
  }
}
