import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroupDirective } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzTabPosition } from 'ng-zorro-antd/tabs';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from 'src/app/core/admin.service';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'zip-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.less'],
})
export class AddCategoryComponent implements OnInit {
  position: NzTabPosition = 'right';
  isEditable = false;

  settings = {
    // hideSubHeader: true,
    actions: {
      position: 'right',
      add: false,
    },
    edit: {
      editButtonContent: `<span class="material-icons">edit</span>`,
      saveButtonContent: '<span class="material-icons">check_box</span>',
      cancelButtonContent: '<span class="material-icons">cancel</span>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: `<span class="material-icons">delete</span>`,
      confirmDelete: true,
    },
    pager: {
      display: false,
    },
    columns: {},
  };

  data = [];

  categoryForm = this.fb.group({
    category: this.fb.array([this.fb.control('')]),
  });

  get category(): any {
    return this.categoryForm.get('category') as FormArray;
  }

  addMoreFields(): any {
    this.category.push(this.fb.control(''));
  }

  removeFields(i: number): any {
    this.category.removeAt(i);
  }

  constructor(
    private fb: FormBuilder,
    private modal: NzModalService,
    private adminService: AdminService,
    private commonService: CommonService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.spinner.show();
    this.adminService.getCategory().subscribe(
      (res: any) => {
        console.log(res);

        this.data = res?.data;
        this.settings = { ...this.settings, columns: res.columnToShow };
        this.settings.columns['category'].width = '85%';

        this.categoryForm.reset();
        this.spinner.hide();
      },
      (err: any) => {
        console.log(err);
        this.settings = { ...this.settings, columns: err.error.columnToShow };
        this.settings.columns['category'].width = '85%';
        this.spinner.hide();
      }
    );
  }

  onSubmit(formDirective: FormGroupDirective): void {
    this.spinner.show();
    const Category = this.categoryForm.value.category;
    let tempArray = [];
    Category.forEach((cat: any) => {
      if (cat.trim() !== '') {
        tempArray.push({ category: cat.trim() });
      }
    });
    if (tempArray.length !== 0) {
      this.spinner.show();
      this.adminService.saveCategory(tempArray).subscribe(
        (res: any) => {
          this.commonService.showSnackBar(res.message);
          formDirective.resetForm();
          this.categoryForm.reset();
        },
        (err: any) => console.log(err)
      );
    }
    this.spinner.hide();
  }

  onRowDelete(event: any): void {
    this.modal.confirm({
      nzTitle: 'Are you sure delete this Category?',
      nzOkText: 'Delete',
      nzOkType: 'danger',
      nzOkDanger: true,
      nzOnOk: () => {
        this.spinner.show();
        this.adminService
          .deleteCategory({
            id: event.data._id,
            SchemaName: 'CategorySchema',
          })
          .subscribe(
            (res: any) => {
              this.spinner.hide();
              this.commonService.showSnackBar(res?.message);
              event.confirm.resolve();
            },
            (err: any) => {
              console.log(err);

              this.spinner.hide();
              this.commonService.showSnackBar(err?.message);
            }
          );
      },
      nzCancelText: 'Cancel',
      nzOnCancel: () => event.confirm.reject(),
    });
  }

  onUpdate(event: any): void {
    if (event?.newData.name !== '') {
      event?.confirm?.resolve();
      this.spinner.show();
      this.adminService
        .updateCategory({
          id: event.newData._id,
          category: event.newData.category,
        })
        .subscribe(
          (res: any) => {
            this.spinner.hide();
            this.commonService.showSnackBar(res.message);
          },
          (err: any) => {
            this.spinner.hide();
            console.log(err);
          }
        );
      //
    } else {
    }
  }
}
