import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/authGuard/auth.guard';
import { AddBranchComponent } from 'src/app/component/add-seller-account/add-seller-account.component';
import { SidebarComponent } from 'src/app/layouts/sidebar/sidebar.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminComponent } from './admin.component';
import { AddCategoryComponent } from './Components/add-category/add-category.component';

const routes: Routes = [
  {
    // pathMatch: 'full',
    component: AdminComponent,
    path: 'dashboard',
    children: [
      {
        path: '',
        component: AdminDashboardComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard],
      },
      {
        path: 'addcategory',
        component: AddCategoryComponent,
        pathMatch: 'full',
      },
    ],
  },
  // {
  //   path: 'addcategory',
  //   component: AddCategoryComponent,
  //   pathMatch: 'full',
  // },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
