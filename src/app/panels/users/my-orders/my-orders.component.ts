import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { map } from 'rxjs/operators';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'zip-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.less'],
})
export class MyOrdersComponent implements OnInit {
  checkOptions = [
    { label: 'Out the way', value: 'On_the_way', checked: false },
    { label: 'Delivered', value: 'delivered', checked: false },
    { label: 'Not Delivered', value: 'Not_delivered', checked: false },
    { label: 'Cancelled', value: 'Cancelled', checked: false },
    { label: 'Returned', value: 'Returned', checked: false },
  ];
  user = this.commonService.decryptData(localStorage.getItem('_crd'));
  isEnableClearbtn = false;
  datasource: any;

  constructor(
    private spinner: NgxSpinnerService,
    private commonService: CommonService
  ) {}

  ngOnInit(): void {
    window.scroll(0, 0);
    this.getData();
  }

  getData(): void {
    this.spinner.show();
    this.commonService.getOrders('customerId', this.user?._id).subscribe(
      (data: any) => {
        this.spinner.hide();
        this.datasource = data?.data;
        this.modifyData();
      },
      (err) => {
        this.spinner.hide();
      }
    );
  }

  modifyData(): void {
    let arr = [];
    let obj: any = {};
    let totalPrice: number;
    let name: Array<string>;
    this.datasource?.forEach((item: any) => {
      arr = [];
      name = [];
      totalPrice = 0;
      item?.orderlist?.forEach((i: any) => {
        obj = {};
        obj.discount = i?.l?.q?.discount;
        obj.orderQuantity = i?.l?.orderQuantity;
        obj.packof = i?.l?.q?.availablein;
        obj.productName = i?.productName;
        obj.productPrice = i?.l?.q?.productprice;
        obj.item = i?.l?.q?.productId;
        name.push(i?.productName);
        totalPrice = totalPrice + i?.l?.q?.productprice;
        arr.push(obj);
        if (arr.length === item?.orderlist?.length) {
          item.orderlist = arr;
          item.name = name;
          item.totalPrice = totalPrice;
          console.log(this.datasource);
        }
      });
    });
  }

  updateChecked(event: any): void {
    const find = event.map((t: any) => t.checked === true);
    if (find.includes(true)) {
      this.isEnableClearbtn = true;
    } else {
      this.isEnableClearbtn = false;
    }
  }

  clearfilterParams(): void {
    this.isEnableClearbtn = false;
    this.checkOptions = [
      { label: 'Out the way', value: 'On_the_way', checked: false },
      { label: 'Delivered', value: 'delivered', checked: false },
      { label: 'Not Delivered', value: 'Not_delivered', checked: false },
      { label: 'Cancelled', value: 'Cancelled', checked: false },
      { label: 'Returned', value: 'Returned', checked: false },
    ];
  }
}
