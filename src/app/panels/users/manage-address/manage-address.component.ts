import { Component, OnInit } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DeliveryAddressFormComponent } from 'src/app/component/delivery-address-form/delivery-address-form.component';
import { CommonService } from 'src/app/core/common.service';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'zip-manage-address',
  templateUrl: './manage-address.component.html',
  styleUrls: ['./manage-address.component.less'],
})
export class ManageAddressComponent implements OnInit {
  user = this.commonService.decryptData(localStorage.getItem('_crd'));
  isFormOpen = false;
  deliveryaddersslist: any;

  constructor(
    private spinner: NgxSpinnerService,
    private usreService: UserService,
    private commonService: CommonService,
    private drawerService: NzDrawerService,
    private nzModal: NzModalService
  ) {}

  ngOnInit(): void {
    this.getAddress();
    this.commonService.ItemAddEvent.subscribe((r: any) => {
      this.getAddress();
    });
  }

  getAddress(): void {
    this.spinner.show();
    this.usreService
      .getDeliveryAddress(this.user?._id)
      .subscribe((add: any) => {
        this.deliveryaddersslist = add?.data;
        this.spinner.hide();
      });
  }

  OnCanel(): void {
    // this.isFormOpen = !this.isFormOpen;
  }
  OnSubmit(): void {
    // this.isFormOpen = !this.isFormOpen;
  }

  onEdit(item: any): void {
    this.drawerService.create({
      nzContent: DeliveryAddressFormComponent,
      nzWidth: 690,
      nzClosable: false,
      // nzMask: false,
      nzKeyboard: false,
      nzMaskClosable: false,
      nzContentParams: {
        rowdata: item,
      },
    });
  }

  delete(id: string): void {
    this.nzModal.confirm({
      nzTitle: 'Are you sure delete this Category?',
      nzOkText: 'Delete',
      nzOkType: 'danger',
      nzOkDanger: true,
      nzOnOk: () => {
        this.spinner.show();
        this.commonService
          .deleteItemsById({
            id,
            SchemaName: 'deliveryAddress',
          })
          // tslint:disable-next-line: deprecation
          .subscribe(
            (res: any) => {
              this.spinner.hide();
              this.commonService.showSnackBar(res.message);
              this.commonService.ItemAddEvent.emit('delete');
            },
            (err: any) => {
              this.spinner.hide();
              this.commonService.showSnackBar(err?.message);
            }
          );
      },
      nzCancelText: 'Cancel',
      nzOnCancel: () => {},
    });
  }

  openForm(): void {
    this.drawerService.create({
      nzContent: DeliveryAddressFormComponent,
      nzWidth: 690,
      nzClosable: false,
      // nzMask: false,
      nzKeyboard: false,
      nzMaskClosable: false,
    });
  }
  // this.isFormOpen = !this.isFormOpen;
}
