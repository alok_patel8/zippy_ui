import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from 'src/app/auth/change-password/change-password.component';
import { CartComponent } from 'src/app/component/cart/cart.component';
import { CheckoutComponent } from 'src/app/component/checkout/checkout.component';
import { PaymentSuccessComponent } from 'src/app/component/payment-success/payment-success.component';
import { RestaurantsComponent } from 'src/app/component/restaurants/restaurants.component';
import { ShopDetailsComponent } from 'src/app/component/shop-details/shop-details.component';
import { ManageAddressComponent } from './manage-address/manage-address.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { UserDashbordComponent } from './user-dashbord/user-dashbord.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

const routes: Routes = [
  {
    path: 'shop',
    component: RestaurantsComponent,
    pathMatch: 'full',
    // children: [

    // ],
  },
  {
    // pathMatch: 'full',
    component: UserDashbordComponent,
    path: 'dashboard',
    children: [
      {
        path: 'profile',
        component: UserProfileComponent,
        pathMatch: 'full',
      },
      {
        path: 'myorders',
        component: MyOrdersComponent,
        pathMatch: 'full'
      },
      {
        path: 'delivery-address',
        component: ManageAddressComponent,
        pathMatch: 'full'
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
        pathMatch: 'full'
      },
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full',
      },
    ]
  },
  {
    path: 'shop/:shopId',
    component: ShopDetailsComponent,
    pathMatch: 'full',
  },
  {
    path: 'cart',
    component: CartComponent,
    pathMatch: 'full',
  },
  {
    path: 'checkout',
    component: CheckoutComponent,
    pathMatch: 'full',
  },
  {
    path: 'payment/success',
    component: PaymentSuccessComponent,
    pathMatch: 'full',
  },
  {
    path: '',
    redirectTo: 'shop',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
