import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'zip-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.less']
})
export class UserProfileComponent implements OnInit {
  isProfileEditable = false;
  isEmailEditable = false;
  isContactEditable = false;

  constructor() { }

  ngOnInit(): void {
    window.scroll(0, 0);
  }

  editUserName(): void{
    this.isProfileEditable = !this.isProfileEditable;
  }
  editUserEmail(): void{
    this.isEmailEditable = !this.isEmailEditable;
  }
  editUserContact(): void{
    this.isContactEditable = !this.isContactEditable;
  }

}
