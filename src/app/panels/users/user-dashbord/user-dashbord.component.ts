import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'zip-user-dashbord',
  templateUrl: './user-dashbord.component.html',
  styleUrls: ['./user-dashbord.component.less'],
})
export class UserDashbordComponent implements OnInit {
  constructor(private commonService: CommonService, private router: Router) {}

  ngOnInit(): void {
    window.scroll(0, 0);
    this.commonService.userLoggedStatus.subscribe((res: any) => {
      if (res) {
        this.router.navigate(['/']);
      }
    });
  }

  logout(): void{
    this.commonService.userLoggedStatus.emit('logout');
  }
}
