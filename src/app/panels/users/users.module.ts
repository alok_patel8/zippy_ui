import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserDashbordComponent } from './user-dashbord/user-dashbord.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AntDesignModule } from 'src/app/ant.design/ant.design.module';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageAddressComponent } from './manage-address/manage-address.component';
import { ZipCommonModule } from 'src/app/component/zip-common/zip-common.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    UserDashbordComponent,
    UserProfileComponent,
    MyOrdersComponent,
    ManageAddressComponent,
  ],
  imports: [
    CommonModule,
    AntDesignModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ZipCommonModule,
    NgxSpinnerModule,
  ],
})
export class UsersModule {}
