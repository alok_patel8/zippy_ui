import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CartService } from 'src/app/core/cart.service';
import { PaymentService } from 'src/app/core/payment.service';

@Component({
  selector: 'zip-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.less'],
})
export class PaymentSuccessComponent implements OnInit {
  session: any;
  cart: any;

  constructor(
    private paymentService: PaymentService,
    private activatedRoute: ActivatedRoute,
    private sppiner: NgxSpinnerService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.sppiner.show();
    this.activatedRoute.queryParams.subscribe((params: any) => {
      this.session = params;
      this.sppiner.hide();
      this.getPaymentSession();
    });
  }

  getPaymentSession(): void {
    this.sppiner.show();
    this.paymentService
      .getPeymentSession(this.session)
      .subscribe((key: any) => {
        this.cartService.userCart.next('');
        this.sppiner.hide();
      });
  }
}
