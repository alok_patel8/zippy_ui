import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { SignInComponent } from 'src/app/auth/sign-in/sign-in.component';
import { CommonService } from 'src/app/core/common.service';
import { environment } from 'src/environments/environment';
import { StripeService } from 'ngx-stripe';
import { PaymentService } from 'src/app/core/payment.service';
import { CartService } from 'src/app/core/cart.service';
import { DeliveryAddressFormComponent } from '../delivery-address-form/delivery-address-form.component';
import { UserService } from 'src/app/core/user.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare let razorPay: any;

@Component({
  selector: 'zip-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.less'],
})
export class CheckoutComponent implements OnInit {
  SERVER_URL = environment.devenvironmentUrl;
  user = this.commonService.decryptData(localStorage.getItem('_crd'));
  selectIndex = 0;
  isUserLoggedIn = false;
  disable = false;
  cartItem: any;

  selectDeliveryAddress = new FormControl('');
  selectedPayment = new FormControl('');
  // selectedPayM: any;
  selectedAddress: any;
  selectedPayM = null;

  deliveryaddersslist = [];
  paymentMethodListd = [
    {
      id: 1,
      name: 'Card (Stripe)',
      code: 'card',
    },
    {
      id: 2,
      name: 'Razor Pay',
      code: 'razorpay',
    },
  ];

  options = {
    key: 'rzp_test_OtEZYvzOID64qu',
    amount: '60000',
    currency: 'INR',
    name: 'Acme Corp',
    description: 'Test Transaction',
    image: 'http://localhost:4200/assets/images/zippyfullicon.png',
    order_id: '',
    handler: (response: any) => {
      console.log(response);

      alert(response.razorpay_payment_id);
      alert(response.razorpay_order_id);
      alert(response.razorpay_signature);
    },
    // callback_url: 'http://localhost:4200/zippy/user/payment/success',
    // redirect: false,
    prefill: {
      name: 'Gaurav Kumar',
      email: 'gaurav.kumar@example.com',
      contact: '9999999999',
    },
    notes: {
      address: 'Razorpay Corporate Office',
    },
    theme: {
      color: '#5bc574',
    },
  };

  // veriable for stripe payment

  stripeForm: FormGroup;
  totalItemInCart: any;
  cratshop: any;

  constructor(
    private authService: AuthService,
    private commonService: CommonService,
    private drawerService: NzDrawerService,
    private fb: FormBuilder,
    private stripeService: StripeService,
    private paymentService: PaymentService,
    private cartService: CartService,
    private usreService: UserService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    window.addEventListener('scroll', () => {
      document.getElementById('appNav').style.position = 'fixed';
    });
    this.isUserLoggedIn = this.authService.isUserLoggedIn();
    this.isUserLoggedIn ? (this.selectIndex = 1) : (this.selectIndex = 0);
    this.cartService.userCart.subscribe((cart: any) => {
      this.cartItem = cart?.data;
      this.cratshop = cart?.shop;
      this.totalItemInCart = cart?.totalCartItem;
    });
    this.commonService.ItemAddEvent.subscribe((r) => {
      this.getAddress();
    });
    this.commonService.userLoggedStatus.subscribe((res: any) => {
      this.isUserLoggedIn = this.authService.isUserLoggedIn();
      this.getAddress();
      if (this.isUserLoggedIn) {
        this.selectIndex = 1;
      } else {
        this.selectIndex = 0;
      }
    });
    this.getAddress();
    this.modifyCardData();
  }

  onChangeClick(index: any): void {
    this.selectIndex = index;
  }
  onNextClick(): void {
    this.selectIndex += 1;
  }

  getAddress(): void {
    if (this.authService.isUserLoggedIn()) {
      this.spinner.show();
      this.usreService.getDeliveryAddress(this.user?._id).subscribe(
        (add: any) => {
          this.deliveryaddersslist = add?.data;
          this.selectDeliveryAddress.setValue(this.deliveryaddersslist[0]);
          this.deliverHere();
          this.spinner.hide();
        },
        (err: any) => {
          this.spinner.hide();
        }
      );
    }
  }

  changeAdderss(): void {
    this.drawerService.create({
      nzContent: DeliveryAddressFormComponent,
      nzWidth: 690,
      nzClosable: false,
      // nzMask: false,
      nzKeyboard: false,
      nzMaskClosable: false,
    });
  }

  openDrawer(formType: any): void {
    this.drawerService.create({
      nzContent: SignInComponent,
      nzTitle: null,
      nzWidth: 450,
      nzClosable: false,
      nzBodyStyle: { padding: 0 },
      nzContentParams: {
        formType,
      },
    });
  }

  logout(): void {
    localStorage.removeItem('_token');
    localStorage.removeItem('_crd');
    this.commonService.userLoggedStatus.emit('logout');
    this.cartService.userCart.next('');
    this.isUserLoggedIn = false;
  }

  deliverHere(): void {
    this.selectedAddress = this.selectDeliveryAddress.value;
  }

  calculateSellingPrice(discountPercent: any, price: any): number {
    const discount = Number((discountPercent / 100) * price);
    return Math.ceil(price - discount);
  }

  selectPaymentMethod(event: any): void {
    this.selectedPayM = event;
  }

  saveOrders(): void {
    if (this.authService.isUserLoggedIn()) {
      this.spinner.show();
      const entity = {
        customerId: this.user?._id,
        shopId: this.cratshop?._id,
        orderlist: this.cartItem,
        orderStatus: 'ONP',
        paymentStatus: 'pending',
        // orderDateTime: Date.now(),
        deliveryAddress: this.selectDeliveryAddress?.value?._id,
      };

      console.log(entity);
    }
  }

  // payment
  initPayment(): void {
    this.disable = true;
    this.spinner.show();
    const entity = {
      customerId: this.user?._id,
      shopId: this.cratshop?._id,
      orderlist: this.cartItem,
      orderStatus: 'ONP',
      paymentStatus: 'pending',
      deliveryAddress: this.selectDeliveryAddress?.value?._id,
      paymentSession: {},
      paymentToken: null,
    };
    // this.commonService.loader.emit(true);
    if (this.selectedPayM?.code === 'card') {
      this.paymentService
        .createPeymentSession(entity)
        .subscribe((session: any) => {
          session = session?.id;
          entity.paymentSession = session;
          entity.paymentToken = session?.id;
          this.cartService.saveOrders(entity).subscribe((ord: any) => {
            this.stripeService
              .redirectToCheckout({ sessionId: session?.id })
              .subscribe((pay: any) => {
                this.spinner.hide();
              });
          });
        });
    } else if (this.selectedPayM?.code === 'razorpay') {
      this.paymentService
        .RazorPayPeymentSession(entity)
        .subscribe((res: any) => {
          this.spinner.hide();
          console.log(res);
          this.options.order_id = res?.id;
          this.options.amount = res?.amount;
          const rzp = new this.paymentService.nativeWindow.Razorpay(
            this.options
          );
          rzp.on('payment.failed', (response: any) => {
            alert(response.error.code);
            alert(response.error.description);
            alert(response.error.source);
            alert(response.error.step);
            alert(response.error.reason);
            alert(response.error.metadata.order_id);
            alert(response.error.metadata.payment_id);
          });
          rzp.open();
          console.log('opened');
        });
    }
  }

  modifyCardData(): void {
    this.cartItem?.forEach((e: any) => {
      e.sellPrice = Math.floor(
        e?.price - Number((e?.discount / 100) * e?.price)
      );
    });
  }
}
