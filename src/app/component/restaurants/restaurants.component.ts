import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/core/common.service';
import { FiltersComponent } from '../filters/filters.component';

@Component({
  selector: 'zip-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.less'],
})
export class RestaurantsComponent implements OnInit {
  cardData = [
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
    // {
    //   title: 'abcd',
    //   description: ['abcd', 'efgh', 'ijk', 'lmn'],
    //   rating: '4.3',
    //   deliveryTime: '23',
    //   rupeeForTow: '230',
    //   couponCode: 'Zippy',
    //   discount: '30',
    // },
  ];
  cardList = [];
  totalRestaurant: any;

  constructor(
    private modal: NzModalService,
    private commonService: CommonService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    window.scroll(0, 0);
    this.getCardData();
    this.totalRestaurant = this.cardData.length;
    const appNav = document.getElementById('appNav');
    const filterNav = document.getElementById('filterNav');
    const sticky = filterNav.offsetTop;
    window.addEventListener('scroll', () => {
      if (window.pageYOffset >= sticky) {
        appNav.style.display = 'none';
        filterNav.style.position = 'fixed';
        filterNav.style.top = '0';
        filterNav.style.width = '100%';
        filterNav.style.zIndex = '9999';
      } else {
        appNav.style.display = 'block';
        filterNav.style.position = 'initial';
        //  filterNav.style.top = '0';
        //  filterNav.style.width = '100%';
      }
    });
  }

  getCardData(): void {
    this.spinner.show();
    this.commonService.getCardData().subscribe((res: any) => {
      this.cardData = res?.data;
      this.modifyData();
      this.spinner.hide();
      console.log('this is res', this.cardData);
    });
  }

  modifyData(): void {
    this.cardList = [];
    let tempArray = [];
    this.cardData.forEach((item) => {
      if (tempArray.length < 4) {
        tempArray.push(item);
      } else {
        this.cardList.push(tempArray);
        tempArray = [];
        tempArray.push(item);
      }
    });
    this.cardList.push(tempArray);
    console.log('cradlist', this.cardList);
  }

  openFilter(): void {
    this.modal.create({
      nzContent: FiltersComponent,
      nzFooter: null,
      nzTitle: 'Filter',
      nzMask: false,
    });
  }
}
