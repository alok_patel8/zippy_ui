import { Component, Input, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { CartService } from 'src/app/core/cart.service';
import { CommonService } from 'src/app/core/common.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'zip-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.less'],
})
export class CardContainerComponent implements OnInit {
  SERVER_URL = environment.devenvironmentUrl;
  cartData = [];
  totalOrderQuantity: any;
  grandTotal: number;
  goToCardBtn = false;
  goToShopBtn = false;
  isCardEditable = true;

  @Input() CardConfig: any;
  cardItemList: string[];
  userID: any;
  loader: boolean;
  shopdetails: any;
  cratshop: any;

  constructor(
    private commonService: CommonService,
    private cartService: CartService,
    private authService: AuthService,
    private nzModal: NzModalService
  ) {}

  ngOnInit(): void {
    this.cartData = JSON.parse(localStorage.getItem('_card'));
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.cartService.userCart.subscribe((items: any) => {
      this.cartData = items?.data;
      this.cratshop = items?.shop;
      this.totalOrderQuantity = items?.totalCartItem;
    });
    this.commonService.ShopData.subscribe((res: any) => {
      this.shopdetails = res;
    });
    this.userID = user?._id;
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnChanges(): void {
    this.goToCardBtn = this.CardConfig?.goToCardBtn;
    this.goToShopBtn = this.CardConfig?.goToShopBtn;
    this.isCardEditable = this.CardConfig?.isCardEditable;
  }

  calculateTotal(actionType: string): number {
    let TotalQuantity = 0;
    const cart = JSON.parse(localStorage.getItem('_card'));
    let grandTotal = 0;
    cart?.forEach((ele: any) => {
      grandTotal = ele?.sellingPrice * ele?.OrderQuantity + grandTotal;
      TotalQuantity += ele?.OrderQuantity;
    });
    this.grandTotal = grandTotal;
    return actionType === 'price' ? grandTotal : TotalQuantity;
  }

  UpdateCardQuantity(id: string, action: string): void {
    if (this.authService.isUserLoggedIn()) {
      this.commonService.loader.emit(true);
      this.cartService
        .UpdateCardData(action, id, this.shopdetails?._id)
        .subscribe((res) => {
          this.cartService.userCart.next(res);
          this.commonService.loader.emit(false);
        });
    } else {
      this.cartService.updateQuantity(action, id);
    }
  }

  RemoveItemFromCard(actionType: any, id: any): void {
    this.nzModal.confirm({
      nzTitle: 'Are you really want to remove this items from Shopping Cart?',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOkText: 'Yes, Remove',
      nzOnOk: () => {
        if (this.authService.isUserLoggedIn()) {
          this.commonService.loader.emit(true);
          this.cartService
            .UpdateCardData(actionType, id, this.shopdetails?._id)
            .subscribe(
              (res) => {
                this.cartService.TotalCartItems.emit(res);
                this.cartService.userCart.next(res);
                this.commonService.loader.emit(false);
              },
              (err) => {
                this.commonService.loader.emit(false);
              }
            );
        } else {
        }
      },
      nzCancelText: 'No',
    });
  }

  changeOrderQuantity(actionType: any, id: any): void {
    this.cartData.forEach((i: any) => {
      if (i?.itemId === id) {
        if (actionType === 'remove') {
          this.cartData?.splice(this.cartData?.indexOf(i), 1);
        }
        if (actionType === 'add' && i.OrderQuantity < 10) {
          i.OrderQuantity += 1;
        } else if (actionType === 'sub' && i.OrderQuantity >= 1) {
          i.OrderQuantity -= 1;
          // this.isItemAvailInCart(id)
          if (i.OrderQuantity === 0) {
            this.cartData?.forEach((ele: any) => {
              if (ele?.OrderQuantity === 0) {
                this.cartData?.splice(this.cartData?.indexOf(ele), 1);
              }
            });
          }
        }
      }
    });
    localStorage.setItem('_card', JSON.stringify(this.cartData));
    this.commonService.ItemAddEvent.emit(id);
  }
}
