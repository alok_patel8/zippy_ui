import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { CartService } from 'src/app/core/cart.service';
import { CommonService } from 'src/app/core/common.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'zip-av-product',
  templateUrl: './av-product.component.html',
  styleUrls: ['./av-product.component.less'],
})
export class AvProductComponent implements OnInit {
  @Input() SelectedProduct: any;
  @Input() cardItemList: any;
  @Input() shopDetails: any;
  SERVER_URL = environment.devenvironmentUrl;
  userID: any;
  zipCart = JSON.parse(localStorage.getItem('cart'));
  orderList = [];
  shopName: any;
  loader = false;

  constructor(
    private commonService: CommonService,
    private authService: AuthService,
    private cartService: CartService,
    private nzModal: NzModalService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.userID = user?._id;
    this.cartService.userCart.subscribe((items: any) => {
      this.zipCart = items?.data;
      this.cardItemList = this.cartService.ItemsInCart(items?.data, 'child');
    });
    console.log(this.SelectedProduct);
    console.log(this.shopDetails);
    console.log(this.zipCart);
  }

  addNewItemInCart(id: any): void {
    if (this.authService.isUserLoggedIn()) {
      this.spinner.show();
      this.cartService
        .UpdateCardData('New', id, this.shopDetails?._id)
        .subscribe((res) => {
          if (res?.code === 'diffrentShop') {
            this.spinner.hide();
            this.removeOldItemsFromCart(res?.message, id);
          } else {
            this.cartService.userCart.next(res);
            this.spinner.hide();
          }
        });
    } else {
      this.cartService.addNewDataInCart(id);
    }
  }

  UpdateCardQuantity(id: string, action: string): void {
    if (this.authService.isUserLoggedIn()) {
      this.spinner.show();
      this.cartService
        .UpdateCardData(action, id, this.shopDetails?._id)
        .subscribe((res) => {
          if (res?.code === 'diffrentShop') {
            this.spinner.hide();
            this.removeOldItemsFromCart(res?.message, id);
          } else {
            this.cartService.userCart.next(res);
            this.spinner.hide();
          }
        });
    } else {
      this.cartService.updateQuantity(action, id);
    }
  }

  getQuantity(id: any): any {
    let s = this.zipCart?.filter((x) => id === x?.l?.q?._id);
    return s[0]?.l?.orderQuantity;
  }

  calculateDiscount(discountPercent: any, price: any): Number {
    const discount = Number((discountPercent / 100) * price);
    return Math.ceil(price - discount);
  }

  removeOldItemsFromCart(message: string, itemId: any): void {
    this.nzModal.confirm({
      nzTitle: 'Replace cart items?',
      nzContent: message,
      nzOkType: 'danger',
      nzOkDanger: true,
      nzOkText: 'Yes, Remove',
      nzOnOk: () => {
        if (this.authService.isUserLoggedIn()) {
          this.spinner.show();
          const obj = {
            customerId: this.userID,
            shopId: this.shopDetails?._id,
            itemId,
          };
          this.cartService.removeAllCartItems(obj).subscribe((res: any) => {
            this.cartService.userCart.next(res);
            this.spinner.hide();
          });
          // this.loading = true;
          // this.cartService
          //   .UpdateCardData(actionType, id, this.shopdetails?._id)
          //   .subscribe(
          //     (res) => {
          //       this.cartService.TotalCartItems.emit(res);
          //       this.cartService.userCart.next(res);
          //       // this.loading = false;
          //     },
          //     (err) => {
          //       // this.loading = false;
          //       this.spinner.hide();
          //     }
          //   );
        } else {
        }
      },
      nzCancelText: 'No',
    });
  }
}
