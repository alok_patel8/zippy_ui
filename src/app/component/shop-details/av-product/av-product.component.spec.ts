import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvProductComponent } from './av-product.component';

describe('AvProductComponent', () => {
  let component: AvProductComponent;
  let fixture: ComponentFixture<AvProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
