import { Component, HostListener, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { AdminService } from 'src/app/core/admin.service';
import { CommonService } from 'src/app/core/common.service';
import { SellerService } from 'src/app/core/seller.service';
import { AvProductComponent } from './av-product/av-product.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CartService } from 'src/app/core/cart.service';

@Component({
  selector: 'zip-shop-details',
  templateUrl: './shop-details.component.html',
  styleUrls: ['./shop-details.component.less'],
})
export class ShopDetailsComponent implements OnInit {
  selectedShopId: any;
  localCart = JSON.parse(localStorage.getItem('_card'));
  shopDetails: any;
  products: Array<any>;
  isStyleClassApply = false;
  scrollValue: any;
  categoryList: any;
  selectedItem: any;
  cartData = [];
  availableCategory = [];
  isVisible = false;
  Recommendation = [];
  selectedItemPrice: any;
  carddata = [];
  grandTotal: any;
  route: string;
  pageYOffSet: number;
  cardItemList: any;
  cardConfig = { goToCardBtn: false, isCardEditable: false };
  userID: any;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(): void {
    const pos =
      (document.documentElement.scrollTop || document.body.scrollTop) +
      document.documentElement.offsetHeight;
    // let max = document.documentElement.scrollHeight;
    // console.log(pos);
    this.scrollValue = pos;

    if (pos >= 700) {
      // document.getElementById('appNav').style.display = 'none';
      // document.getElementById('shopbanner').style.position = 'fixed';
      // document.getElementById('leftBox').style.position = 'fixed';
      // document.getElementById('rightBox').style.position = 'fixed';
      // document.getElementById('shopbanner').style.top = '0';
      // document.getElementById('content').style.marginTop = '0px';
      // this.isStyleClassApply = true;
    } else {
      // document.getElementById('appNav').style.display = 'block';
      // document.getElementById('shopbanner').style.position = 'absolute';
      // document.getElementById('shopbanner').style.top = '78px';
      // document.getElementById('content').style.marginTop = '100px';
      // document.getElementById('leftBox').style.position = 'absolute';
      // document.getElementById('rightBox').style.position = 'absolute';
      // this.isStyleClassApply = false;
    }
  }
  constructor(
    private activatedRouter: ActivatedRoute,
    private commonService: CommonService,
    private sellerService: SellerService,
    private adminservice: AdminService,
    private spinner: NgxSpinnerService,
    private modalService: NzModalService,
    private nzdrawer: NzDrawerService,
    private authService: AuthService,
    private bottomSheet: MatBottomSheet,
    private cartService: CartService,
    private router: Router
  ) {
    this.activatedRouter.params.subscribe((res: any) => {
      this.selectedShopId = res.shopId;
      this.getShopData();
    });
    this.route = this.router.url;
  }

  ngOnInit(): void {
    window.scroll(0, 0);
    window.addEventListener('scroll', () => {
      document.getElementById('appNav').style.position = 'absolute';
      const navbar = document.getElementById('navbar');
      const sticky = navbar.offsetTop;
      this.pageYOffSet = window.pageYOffset;
      if (window.pageYOffset >= sticky) {
        navbar.classList.add('sticky');
      } else {
        navbar.classList.remove('sticky');
      }
    });
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.userID = user?._id;
    this.commonService.ItemAddEvent.subscribe((res: any) => {
      this.cartData = JSON.parse(localStorage.getItem('_card'));
    });
    this.cardConfig.goToCardBtn = true;
    this.cardConfig.isCardEditable = true;
    this.getCategoryList();
    this.cartService.userCart.subscribe((items: any) => {
      this.cardItemList = this.cartService.ItemsInCart(items?.data, 'parent');
    });
  }

  getShopData(): void {
    // this.commonService.loader.emit(true);
    this.spinner.show();
    this.products = [];
    this.commonService.getShopDataById(this.selectedShopId).subscribe(
      (res: any) => {
        this.products = res?.data;
        this.shopDetails = res?.shop[0];
        // this.commonService.loader.emit(false);
        this.spinner.hide();
        console.log('shop data', this.products);
        console.log('shop data', this.shopDetails);

        this.commonService.ShopData.next(this.shopDetails);
        this.modifyProductAccordingToCategory();
        this.getCardData();
      },
      (err: any) => {
        console.log(err);
        this.spinner.hide();
      }
    );
  }

  getCategoryList(): void {
    this.adminservice.getCategory().subscribe((res: any) => {
      this.categoryList = res?.data;
    });
  }

  changeOrderQuantity(actionType: any, id: any): void {
    this.cartData.forEach((i: any) => {
      if (i?.itemId === id) {
        if (actionType === 'remove') {
          this.cartData?.splice(this.cartData?.indexOf(i), 1);
        }
        if (actionType === 'add' && i.OrderQuantity < 10) {
          i.OrderQuantity += 1;
        } else if (actionType === 'sub' && i.OrderQuantity >= 1) {
          i.OrderQuantity -= 1;
          if (i.OrderQuantity === 0) {
            this.cartData?.forEach((ele: any) => {
              if (ele?.OrderQuantity === 0) {
                this.cartData?.splice(this.cartData?.indexOf(ele), 1);
              }
            });
          }
        }
      }
    });
    localStorage.setItem('_card', JSON.stringify(this.cartData));
    this.cartData = JSON.parse(localStorage.getItem('_card'));
    this.commonService.OrederList.emit(this.cartData);
  }

  calculateGrandTotal(actionType: string): number {
    let TotalQuantity = 0;
    const cart = JSON.parse(localStorage.getItem('_card'));
    this.grandTotal = 0;
    cart?.forEach((ele: any) => {
      this.grandTotal =
        ele?.sellingPrice * ele?.OrderQuantity + this.grandTotal;
      TotalQuantity += ele?.OrderQuantity;
    });
    this.commonService.OrederList.emit(cart);
    return actionType === 'price' ? this.grandTotal : TotalQuantity;
  }

  onselectQuantity(selecteddata?: any): void {
    this.selectedItemPrice = selecteddata?.productprice;
  }

  getCategoryName(categoryId: any): any {
    let name;
    this.categoryList?.map((ele) => {
      if (ele?._id === categoryId) {
        name = ele?.category;
        if (this.Recommendation.indexOf(name) === -1) {
          this.Recommendation.push(name);
          this.selectedItem = this.Recommendation[0];
        }
      }
    });
    return name;
  }

  modifyProductAccordingToCategory(): void {
    const tempArray = [];
    this.products?.forEach((prod) => {
      const categoryId = prod?.categoryName;
      if (this.availableCategory.indexOf(prod?.categoryName) === -1) {
        this.availableCategory.push(prod?.categoryName);
        tempArray.push({ categoryId, data: [prod] });
      } else {
        tempArray.forEach((item) => {
          if (item?.categoryId === categoryId) {
            item?.data.push(prod);
          }
        });
      }
    });
    this.products = tempArray;
  }

  addQuantity(qS: any): void {
    this.nzdrawer.create({
      nzContent: AvProductComponent,
      nzPlacement: 'left',
      nzWidth: 450,
      nzBodyStyle: {},
      nzClosable: false,
      nzContentParams: {
        SelectedProduct: qS,
        shopDetails: this.shopDetails,
        cardItemList: this.cardItemList,
      },
    });
  }

  getCardData(): void {
    const tempArray = [];
    if (this.authService.isUserLoggedIn()) {
    } else {
      this.cartData = JSON.parse(localStorage.getItem('_card'));
      this.cartData?.forEach((e: any) => {
        this.products?.forEach((item: any) => {
          item?.quantitySet?.forEach((p: any) => {
            if (p?._id === e?.itemId) {
              tempArray.push({
                productName: item?.productName,
                sellingPrice: p?.sellingPrice,
                quantity: e?.OrderQuantity,
                weight: p?.availablein,
              });
            }
          });
        });
      });
    }
  }

  isItemAvailInCart(productId: any): string {
    let s;
    this.carddata?.map((ele: any) => {
      if (ele?.productId === productId) {
        s = true;
      }
    });
    return s ? 'Customize' : 'Add';
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  moveTop(): void {
    window.scroll(0, 0);
  }

  // set card data from localStorae
  // updateCard(): void {
  //   let card = JSON.parse(localStorage.getItem(''))
  //   if (this.authService.isUserLoggedIn()) {
  //   } else {
  //   }
  // }
}
