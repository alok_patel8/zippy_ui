import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { SellerService } from 'src/app/core/seller.service';
import { v4 as uuidv4 } from 'uuid';

import { environment } from 'src/environments/environment';
import { CommonService } from 'src/app/core/common.service';

// import { GoogleMap } from '@googlemaps/map-loader';

const googleMapsAPIKey = 'AIzaSyDuXJtQ3KVgK3dqxCYRRx1wCqCnaijT7QA';
declare let google: any;

@Component({
  selector: 'zip-add-seller-account',
  templateUrl: './add-seller-account.component.html',
  styleUrls: ['./add-seller-account.component.less'],
})
export class AddBranchComponent implements OnInit {
  current = 0;
  passwordVisible = false;
  passwordVisibleConfirm = false;
  password?: string;
  stateList = [];
  cityList = [];

  public branchDetails: FormGroup;
  addresses: FormArray;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private sellerService: SellerService,
    private commonService: CommonService
  ) {
    this.branchDetails = this.fb.group({
      addresses: this.fb.array([this.createAddress()]),
    });
  }

  shopForm = this.fb.group({
    shopname: ['', [Validators.required]],
    shopContactNumber: [
      '',
      [Validators.required, Validators.minLength(10), Validators.maxLength(12)],
    ],
    openTime: [null, [Validators.required]],
    closeTime: [null, [Validators.required]],
    address: [null, [Validators.required]],
    state: [null, [Validators.required]],
    city: [null, [Validators.required]],
    landmark: [null],
    pincode: [
      null,
      [Validators.required, Validators.minLength(6), Validators.maxLength(6)],
    ],
  });
  // Validators.pattern('(0/91)?[7-9][0-9]{9}')

  loginForm = this.fb.group({
    shopId: [uuidv4().split('-')[4]],
    ownerName: [null, [Validators.required]],
    ownerNumber: [
      null,
      [Validators.required, Validators.minLength(10), Validators.maxLength(12)],
    ],
    ownerEmail: [null, [Validators.required, Validators.email]],
    password: [null, [Validators.required, Validators.minLength(6)]],
  });

  get brancherr() {
    return this.shopForm.controls;
  }

  get addresserr() {
    return this.branchDetails.controls;
  }
  get loginserr() {
    return this.loginForm.controls;
  }

  createAddress(): FormGroup {
    return this.fb.group({
      branchname: ['', Validators.required],
      branchId: [uuidv4().split('-')[4], Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      landmark: [''],
      contactNumber: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    document.getElementById('header').style.display = 'none';
    this.spinner.show();
    environment.StateCity.states.forEach((ele: any) => {
      this.stateList.push(ele?.state);
      this.spinner.hide();
    });
  }

  selectCities(): void {
    if (this.shopForm.controls.state.value) {
      this.spinner.show();
      environment.StateCity.states.forEach((ele: any) => {
        if (ele?.state === this.shopForm.controls.state.value) {
          this.cityList = ele?.districts;
          this.spinner.hide();
        }
      });
    }
  }

  selectformCities(index: any): void {
    if (this.branchDetails.value.addresses[index].state) {
      this.spinner.show();
      environment.StateCity.states.forEach((ele: any) => {
        if (ele?.state === this.branchDetails.value.addresses[index].state) {
          this.cityList = ele?.districts;
          this.spinner.hide();
        }
      });
    }
  }

  pre(): void {
    if (this.current !== 0) {
      this.current -= 1;
    } else {
      this.router.navigate(['./']);
    }
  }

  next(): void {
    this.current += 1;
  }

  addAddress(): void {
    this.addresses = this.branchDetails.get('addresses') as FormArray;
    this.addresses.push(this.createAddress());
  }

  removeAddress(i: number) {
    this.addresses.removeAt(i);
  }

  get branchaddresserr(): FormArray {
    return this.branchDetails.get('addresses') as FormArray;
  }

  get addressControls() {
    // tslint:disable-next-line: no-string-literal
    return this.branchDetails.get('addresses')['controls'];
  }

  onSubmit(): void {
    console.log('onSubmit');

    if (this.shopForm.status === 'VALID' && this.loginForm.status === 'VALID') {
      console.log('onSubmit');

      // console.log('this is branch', this.branchDetails.value);

      const tempObj = {};
      for (const key in this.shopForm.value) {
        if (this.shopForm.value[key]) {
          tempObj[`${key}`] = this.shopForm.value[key];
        }
      }
      for (const key in this.loginForm.value) {
        if (this.loginForm.value[key]) {
          tempObj[`${key}`] = this.loginForm.value[key];
        }
      }

      this.sellerService
        .registration({ tempObj, branch: this.branchDetails.value.addresses })
        .subscribe(
          (res: any) => {
            if (res) {
              this.commonService.showSnackBar(res.msg);
              this.router.navigate(['/zippy/user']);
            }
          },
          (err: any) => {
            console.log(err);
          }
        );
    }
  }

  // drawMap(): void {
  //   console.log('this is map');

  //   const mapLoader = new GoogleMap();
  //   mapLoader.initMap(this.mapLoaderOptions).then((googleMap: any) => {
  //     this.map = googleMap;
  //     const locationButton = document.createElement('button');
  //     locationButton.textContent = 'Current Location';
  //     // locationButton.classList.add('custom-map-control-button');

  //     this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(
  //       locationButton
  //     );
  //     this.getCurrentLocation(locationButton);
  //     this.getLocation();
  //     this.map.addListener('click', (mapsMouseEvent) => {
  //       const latlng = JSON.parse(
  //         JSON.stringify(mapsMouseEvent.latLng, null, 2)
  //       );
  //       this.position.lat = latlng.lat;
  //       this.position.lng = latlng.lng;
  //       // Close the current InfoWindow.

  //       // Create a new InfoWindow.

  //       console.log();
  //     });
  //   });
  // }

  // getCurrentLocation(getLocation: any): void {
  //   getLocation.addEventListener('click', () => {
  //     this.getLocation();
  //   });
  // }

  // getLocation(): void {
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(
  //       (position: any) => {
  //         const pos = {
  //           lat: position.coords.latitude,
  //           lng: position.coords.longitude,
  //         };
  //         console.log('location', pos);
  //         const marker = new google.maps.Marker({
  //           position: pos,
  //           map: this.map,
  //         });
  //         this.position = pos;
  //         this.map.setCenter(pos);
  //         this.map.setZoom(18);
  //         const geocoder = new google.maps.Geocoder();
  //         // this.geocodeLatLng(geocoder, this.map, pos);
  //       },
  //       (err) => {
  //         console.log(err);
  //       }
  //     );
  //   }
  // }

  // geocodeLatLng(
  //   geocoder: google.maps.Geocoder,
  //   map: google.maps.Map,
  //   pos: any
  // ): void {
  //   const input = (document.getElementById('google_map') as HTMLInputElement)
  //     .value;
  //   const latlng = {
  //     lat: pos.lat,
  //     lng: pos.lng,
  //   };
  //   geocoder.geocode(
  //     { location: latlng },
  //     (
  //       results: google.maps.GeocoderResult[],
  //       status: google.maps.GeocoderStatus
  //     ) => {
  //       if (status === 'OK') {
  //         if (results[0]) {
  //           map.setZoom(11);
  //           const marker = new google.maps.Marker({
  //             position: latlng,
  //             map: this.map,
  //           });
  //           window.alert(results[0].formatted_address);
  //         } else {
  //           window.alert('No results found');
  //         }
  //       } else {
  //         window.alert('Geocoder failed due to: ' + status);
  //       }
  //     }
  //   );
  // }
}
