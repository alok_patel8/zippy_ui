import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { CartService } from 'src/app/core/cart.service';
import { CommonService } from 'src/app/core/common.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'zip-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less'],
})
export class CartComponent implements OnInit {
  SERVER_URL = environment.devenvironmentUrl;
  cartItems = [];
  loading = false;
  totalItemsInCart = 0;
  shopdetails: any;

  constructor(
    private commonService: CommonService,
    private nzModal: NzModalService,
    private authService: AuthService,
    private cartService: CartService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    if (this.authService.isUserLoggedIn()) {
      this.spinner.show();
      this.cartService.userCart.subscribe((cart: any) => {
        this.cartItems = cart?.data;
        this.spinner.hide();
        this.totalItemsInCart = cart?.totalCartItem;
      });
    }
    this.commonService.ShopData.subscribe((res: any) => {
      this.shopdetails = res;
    });
    window.scroll(0, 0);
    window.addEventListener('scroll', () => {
      document.getElementById('appNav').style.position = 'fixed';
    });
  }

  RemoveItemFromCard(actionType: any, id: any): void {
    this.nzModal.confirm({
      nzTitle: 'Are you really want to remove this items from Shopping Cart?',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOkText: 'Yes, Remove',
      nzOnOk: () => {
        if (this.authService.isUserLoggedIn()) {
          // this.loading = true;
          this.spinner.show();
          this.cartService
            .UpdateCardData(actionType, id, this.shopdetails?._id)
            .subscribe(
              (res) => {
                this.cartService.TotalCartItems.emit(res);
                this.cartService.userCart.next(res);
                // this.loading = false;
                this.spinner.hide();
              },
              (err) => {
                // this.loading = false;
                this.spinner.hide();
              }
            );
        } else {
          // this.cartItems?.splice(this.cartItems?.indexOf(i), 1);
          localStorage.setItem('_card', JSON.stringify(this.cartItems));
          this.cartItems = JSON.parse(localStorage.getItem('_card'));
          this.commonService.ItemAddEvent.emit(id);
        }
      },
      nzCancelText: 'No',
    });
    this.cartItems = this.cartItems;
    localStorage.setItem('_card', JSON.stringify(this.cartItems));
    this.commonService.ItemAddEvent.emit(id);
  }

  UpdateCardQuantity(id: string, action: string): void {
    if (this.authService.isUserLoggedIn()) {
      // this.loading = true;
      this.spinner.show();
      this.cartService
        .UpdateCardData(action, id, this.shopdetails?._id)
        .subscribe(
          (res) => {
            // this.cartService.ItemsInCart(res?.data);
            this.cartService.TotalCartItems.emit(res);
            this.cartService.userCart.next(res);
            // this.loading = false;
            this.spinner.hide();
          },
          (err) => {
            // this.loading = false;
            this.spinner.hide();
          }
        );
    } else {
      this.cartService.updateQuantity(action, id);
    }
  }

  calculateSellingPrice(discountPercent: any, price: any): number {
    const discount = Number((discountPercent / 100) * price);
    return Math.ceil(price - discount);
  }
}
