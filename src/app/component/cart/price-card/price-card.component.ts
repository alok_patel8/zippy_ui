import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { CartService } from 'src/app/core/cart.service';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'zip-price-card',
  templateUrl: './price-card.component.html',
  styleUrls: ['./price-card.component.less'],
})
export class PriceCardComponent implements OnInit {
  cartItems = JSON.parse(localStorage.getItem('_card'));
  discountPrice = 0;
  totalPrice: number;
  sellingPrice: any;
  deliveryCharge: number;
  totalDiscount: number;
  totalSellingPrice: number;
  totalCartItem: any;

  constructor(
    private commonService: CommonService,
    private authService: AuthService,
    private cartService: CartService) {}

  ngOnInit(): void {
    if (this.authService.isUserLoggedIn()) {
      this.cartService.userCart.subscribe((cart: any) => {
        this.cartItems = cart?.data;
        this.totalCartItem = cart?.totalCartItem;
        this.calculateAmount();
      });
    }
    this.calculateDeliveryCharge();
  }

  calculateAmount(): void{
    this.totalDiscount = 0;
    this.totalSellingPrice = 0;
    this.totalPrice = 0;
    this.cartItems?.forEach((e: any) => {
      this.totalDiscount = this.totalDiscount + Math.floor((e?.l?.q?.discount / 100) * e?.l?.q?.productprice) * e?.l?.orderQuantity;
      this.totalPrice = this.totalPrice + e?.l?.q?.productprice * e?.l?.orderQuantity;
    });
    this.totalSellingPrice = Math.floor(this.totalPrice - this.totalDiscount);
  }

  calculateDeliveryCharge(): number {
    return (this.deliveryCharge = 1);
  }
}
