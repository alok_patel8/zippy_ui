import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryAddressFormComponent } from '../delivery-address-form/delivery-address-form.component';
import { AntDesignModule } from 'src/app/ant.design/ant.design.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [DeliveryAddressFormComponent],
  imports: [
    CommonModule,
    AntDesignModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ],
  exports: [DeliveryAddressFormComponent],
})
export class ZipCommonModule {}
