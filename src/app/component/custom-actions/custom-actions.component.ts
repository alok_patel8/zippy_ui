import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from 'src/app/core/admin.service';
import { CommonService } from 'src/app/core/common.service';
import { SellerService } from 'src/app/core/seller.service';

@Component({
  selector: 'zip-custom-actions',
  templateUrl: './custom-actions.component.html',
  styleUrls: ['./custom-actions.component.less'],
})
export class CustomActionsComponent implements OnInit {
  @Input() rowData: any;
  className: any;
  constructor(
    private sellerServices: SellerService,
    private spinner: NgxSpinnerService,
    private adminService: AdminService,
    private modal: NzModalService,
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private draweService: NzDrawerService
  ) {
    this.route.params.subscribe((params: any) => {
      this.className = params.className;
    });
  }

  ngOnInit(): void {
    // console.log('rowdata', this.rowData);
  }

  onCustomAction(action: any, itemId: any): void {
    switch (action) {
      case 'edit':
        {
          this.router.navigate(['/zippy/seller/addproduct'], {
            queryParams: { id: itemId },
          });
        }
        break;
      case 'delete': {
        this.modal.confirm({
          nzTitle: 'Are you sure delete this Category?',
          nzOkText: 'Delete',
          nzOkType: 'danger',
          nzOkDanger: true,
          nzOnOk: () => {
            this.spinner.show();
            this.commonService
              .deleteItemsById({
                id: itemId,
                SchemaName: 'ProductsSchema',
              })
              // tslint:disable-next-line: deprecation
              .subscribe(
                (res: any) => {
                  this.spinner.hide();
                  this.commonService.showSnackBar(res.message);
                  this.commonService.deleteEvent.emit('delete');
                },
                (err: any) => {
                  this.spinner.hide();
                  this.commonService.showSnackBar(err?.message);
                }
              );
          },
          nzCancelText: 'Cancel',
          nzOnCancel: () => {},
        });
        break;
      }
      case 'received-orders':
        {
          this.commonService.rowdata.next(itemId);
          this.router.navigate([
            'zippy/seller/dashboard/order-details/' + itemId?._id,
          ]);
        }
        break;

      default:
        break;
    }
  }
}
