import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'zip-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.less'],
})
export class FiltersComponent implements OnInit {
  @Input() toDisplayContent: string;
  @Input() categoryList: any;
  availableIn = [
    '100 gm',
    '150 gm',
    '200 gm',
    '250 gm',
    '400 gm',
    '500 gm',
    '1 kg',
    '5 kg',
    '10 kg',
    '20 kg',
    '50 kg',
  ];

  discount = [
    5,
    10,
    15,
    20,
    25,
    30,
    35,
    40,
    45,
    50,
    55,
    65,
    70,
    75,
    85,
    90,
    95,
  ];

  constructor() {}
  customDiscountValue: FormControl = new FormControl('');

  ngOnInit(): void {
    console.log('kjhksa', this.categoryList);
    console.log('aaasdsad', this.toDisplayContent);
  }
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
  }
}
