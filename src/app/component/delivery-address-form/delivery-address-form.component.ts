import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from 'src/app/core/common.service';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'zip-delivery-address-form',
  templateUrl: './delivery-address-form.component.html',
  styleUrls: ['./delivery-address-form.component.less'],
})
export class DeliveryAddressFormComponent implements OnInit {
  // isFormOpen = false;
  user: any;
  isEdit = false;
  @Input() rowdata: any = null;
  @Output() cancel: any = new EventEmitter();
  @Output() Submit: any = new EventEmitter();

  form: FormGroup = this.fb.group({
    customerId: [null, [Validators.required]],
    name: [null, [Validators.required]],
    mobileNumber: [null, Validators.required],
    state: [null, Validators.required],
    city: [null, Validators.required],
    address: [null, Validators.required],
    postalCode: [
      null,
      [Validators.required, Validators.pattern('^[1-9]{1}[0-9]{5}$')],
    ],
    locality: [null],
    landmark: [null],
    alternateNumber: [null],
  });
  pincode: any;

  get err(): any {
    return this.form.controls;
  }
  constructor(
    private fb: FormBuilder,
    private nzdrawerRef: NzDrawerRef,
    private userService: UserService,
    private commonService: CommonService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.form.patchValue({ customerId: this.user?._id });
    if (this.rowdata) {
      this.setForm(this.rowdata);
      this.isEdit = true;
    }
  }

  setForm(i: any): void {
    this.form.setValue({
      customerId: i?.customerId,
      name: i?.name,
      mobileNumber: i?.mobileNumber,
      postalCode: i?.postalCode,
      city: i?.city,
      state: i?.state,
      alternateNumber: i?.alternateNumber,
      address: i?.address,
      locality: i?.locality,
      landmark: i?.landmark,
    });
  }

  OnSubmit(): void {
    this.Submit.emit();
    if (this.form.valid && !this.isEdit) {
      this.spinner.show();
      this.userService.saveDeliverAddress(this.form.value).subscribe(
        (res: any) => {
          this.spinner.hide();
          this.nzdrawerRef.close();
          this.commonService.showSnackBar(res.message);
          this.commonService.ItemAddEvent.emit('itemAdded');
        },
        (err: any) => {
          this.spinner.hide();
          this.commonService.showSnackBar(err?.message);
        }
      );
    } else if (this.form.valid && this.isEdit) {
      this.spinner.show();
      this.userService
        .updateDeliverAddress(this.rowdata?._id, this.form.value)
        .subscribe(
          (res: any) => {
            this.spinner.hide();
            this.commonService.showSnackBar(res.message);
            this.nzdrawerRef.close();
            this.commonService.ItemAddEvent.emit('itemAdded');
          },
          (err: any) => {
            this.spinner.hide();
            this.commonService.showSnackBar(err?.message);
          }
        );
    }
  }
  OnCancel(): void {
    this.nzdrawerRef.close();
    this.cancel.emit();
  }

  getPostalCodeDetails(): void {
    if (this.form.value.postalCode.length === 6) {
      this.spinner.show();
      this.userService.getPinCodeDetails(this.form.value.postalCode).subscribe(
        (res: any) => {
          this.form.patchValue({
            state: res?.data[0]?.stateName,
            city: res?.data[0]?.districtName,
          });
          this.spinner.hide();
          this.pincode = res;
        },
        (err: any) => {
          this.spinner.hide();
          this.commonService.showSnackBar(err?.error?.message);
        }
      );
    }
  }
}
