import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomActionColumnComponent } from './custom-action-column.component';

describe('CustomActionColumnComponent', () => {
  let component: CustomActionColumnComponent;
  let fixture: ComponentFixture<CustomActionColumnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomActionColumnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomActionColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
