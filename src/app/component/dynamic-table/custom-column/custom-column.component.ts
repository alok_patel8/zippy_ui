import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'zip-custom-column',
  templateUrl: './custom-column.component.html',
  styleUrls: ['./custom-column.component.less'],
})
export class CustomColumnComponent implements OnInit {
  className: string;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe((params: any) => {
      this.className = params.className;
    });
  }

  ngOnInit(): void {}
}
