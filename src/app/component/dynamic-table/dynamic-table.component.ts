import { DatePipe } from '@angular/common';
import { htmlAstToRender3Ast } from '@angular/compiler/src/render3/r3_template_transform';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from 'src/app/core/admin.service';
import { CommonService } from 'src/app/core/common.service';
import { SellerService } from 'src/app/core/seller.service';
import { CustomActionsComponent } from '../custom-actions/custom-actions.component';
import { FiltersComponent } from '../filters/filters.component';
import * as moment from 'moment';
import { ImageComponent } from '../image/image.component';
import { CustomActionColumnComponent } from './custom-action-column/custom-action-column.component';

@Component({
  selector: 'zip-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.less'],
})
export class DynamicTableComponent implements OnInit {
  user = this.commonService.decryptData(localStorage.getItem('_crd'));
  currentuserId = this.user?._id;
  tableSetting: any;
  tableData = [];
  category: any;
  className: any;
  isFilterEnable: boolean;
  orderList: any;
  datePipeString: string;

  constructor(
    private sellerServices: SellerService,
    private spinner: NgxSpinnerService,
    private adminService: AdminService,
    private modal: NzModalService,
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private draweService: NzDrawerService
  ) {
    this.route.params.subscribe((params: any) => {
      this.className = params.className;
    });
  }

  ngOnInit(): void {
    window.scroll(0, 0);
    this.tableSetting = {
      hideSubHeader: true,
      actions: {
        position: 'right',
        add: false,
        edit: false,
        delete: false,
        custom: [],
      },
      pager: {
        display: false,
      },
      columns: {},
    };
    this.getTabaleData();
    this.commonService.deleteEvent.subscribe((res: Response) => {
      this.getTabaleData();
    });
  }

  getTabaleData(): any {
    switch (this.className) {
      case 'received-orders':
        {
          this.spinner.show();
          this.commonService.getOrders('shopId', this.currentuserId).subscribe(
            (orders: any) => {
              this.tableData = orders?.data;
              this.tableSetting = {
                ...this.tableSetting,
                columns: orders?.columnToShow,
              };
              this.spinner.hide();
              this.setCustomColumnValue(this.tableSetting?.columns);
              this.setCustomActionTable();
              // this.modifyTableData();
            },
            (err: any) => {
              this.showError(err);
            }
          );
        }
        break;

      default:
        this.spinner.show();
        this.adminService.getCategory().subscribe((res: any) => {
          this.category = res.data;
          this.spinner.hide();
        });
        break;
    }
  }

  showError(err: any): void {
    this.spinner.hide();
    this.commonService.showSnackBar(err?.error?.message);
  }

  setCustomActionTable(): void {
    this.tableSetting = Object.assign(
      {},
      {
        noDataMessage: 'No Data Found',
        mode: 'external',
        hideSubHeader: true,
        actions: false,
        columns: this.tableSetting.columns,
        pager: {
          display: false,
        },
      }
    );
    this.tableSetting.columns.CustomActions = {
      title: 'Actions',
      type: 'custom',
      filter: false,
      valuePrepareFunction: (value: any, row: any, cell: any) => {
        return { value, row, cell };
      },
      renderComponent: CustomActionsComponent,
      onComponentInitFunction(instance: any): void {},
    };
  }

  modifyTableData(): void {
    this.spinner.show();
    switch (this.className) {
      case 'received-orders':
        {
          const arr = [];
          let arrord = [];
          let obj: any = {};
          let ord: any = {};
          this.tableData?.forEach((item: any) => {
            obj = {};
            arrord = [];
            item?.orderlist?.forEach((i: any) => {
              ord = {};
              ord.discount = i?.l?.q?.discount;
              ord.orderQuantity = i?.l?.orderQuantity;
              ord.packof = i?.l?.q?.availablein;
              ord.productName = i?.productName;
              ord.productPrice = i?.l?.q?.productprice;
              ord.item = i?.l?.q?.productId;
              arrord.push(ord);
              if (arrord.length === item?.orderlist?.length) {
                obj.orderlist = arrord;
              }
            });
            obj.customerId = item?.customerId;
            obj.deliveryAddress = item?.deliveryAddress;
            obj.orderDateTime = item?.orderDateTime;
            obj.orderStatus = item?.orderStatus;
            obj.shopId = item?.shopId;
            obj.paymentStatus = item?.paymentStatus;
            obj.deliveryDateTime = item?.deliveryDateTime;
            obj._id = item?._id;
            arr.push(obj);
            if (arr.length === this.tableData.length) {
              this.tableData = arr;
              this.setCustomColumnValue(this.tableSetting?.columns);
              this.spinner.hide();
            }
          });
        }
        break;

      default:
        this.spinner.hide();
        break;
    }
  }

  setCustomColumnValue(cols: any): void {
    for (const key in cols) {
      if (Object.prototype.hasOwnProperty.call(cols, key)) {
        if (key === 'categoryName') {
          // const cols[key] = cols[key];
          this.tableSetting.columns[key] = {
            title: cols[key].title,
            valuePrepareFunction: (value: any) => {
              const cat = this.category?.filter(
                (item: any) => item?._id === value
              );
              return cat[0]?.category;
            },
          };
        }
        if (key === 'orderlist') {
          this.tableSetting.columns[key] = {
            title: cols[key].title,
            type: 'html',
            valuePrepareFunction: (value: any) => {
              const l = value.length;
              return `<span>${l > 1 ? l + ' Items' : l + ' Item'}</span>`;
            },
          };
        }
        if (key === 'deliveryAddress') {
          this.tableSetting.columns[key] = {
            title: cols[key].title,
            type: 'html',
            valuePrepareFunction: (cell: any, row: any) => {
              return row?.deliveryAddress_desc?.name;
            },
          };
        }
        if (key === 'orderStatus') {
          this.tableSetting.columns[key] = {
            title: cols[key].title,
            type: 'html',
            valuePrepareFunction: (value: any) => {
              let resetValue: string;
              if (value === 'ONP') {
                resetValue = 'order not placed';
              }
              if (value === 'done') {
                resetValue = 'order placed';
              }
              const p = `
              <div class="d-flex align-items-center">
              <span class="dot mx-1 ${value}"></span>
              <span>${resetValue}</span>
              </div>
              `;
              return p;
            },
          };
        }
        if (key === 'orderDateTime') {
          this.tableSetting.columns[key] = {
            title: cols[key].title,
            type: 'html',
            valuePrepareFunction: (value: any) => {
              const dateFormat = 'MMM, DD.YYYY hh:mm a';
              const testDateUtc = moment.utc(value);
              const lastLoginDateValue = testDateUtc.local();
              if (value) {
                return `<span>${lastLoginDateValue.format(dateFormat)}</span?`;
              }
            },
          };
        }
        if (key === 'discount') {
          this.tableSetting.columns[key] = {
            title: cols[key].title,
            type: 'html',
            valuePrepareFunction: (cell: any, row: any) => {
              return `<span >${Math.ceil(
                cell ? row?.productprice - row?.sellingPrice : 0
              )} (${cell ? cell : 'N/A'}  ${cell ? '%' : ''})</span>`;
            },
          };
        }
      }
    }
  }

  // onRowSelect(event): void {
  //   console.log('onRowSelect($event)', event);
  // }
  onCustomAction(event: any): void {
    switch (event?.action) {
      case 'edit':
        {
          // [routerLink]="[ '/path', routeParam ]"
          this.router.navigate(['/zippy/seller/addproduct'], {
            queryParams: { id: event?.data._id },
          });
        }
        break;
      case 'delete': {
        this.modal.confirm({
          nzTitle: 'Are you sure delete this Category?',
          nzOkText: 'Delete',
          nzOkType: 'danger',
          nzOkDanger: true,
          nzOnOk: () => {
            this.spinner.show();
            this.commonService
              .deleteItemsById({
                id: event.data._id,
                SchemaName: 'ProductsSchema',
              })
              // tslint:disable-next-line: deprecation
              .subscribe(
                (res: any) => {
                  this.spinner.hide();
                  this.commonService.showSnackBar(res.message);
                  this.commonService.deleteEvent.emit('delete');
                },
                (err: any) => {
                  this.spinner.hide();
                  this.commonService.showSnackBar(err?.message);
                }
              );
          },
          nzCancelText: 'Cancel',
          nzOnCancel: () => {},
        });
        break;
      }
      case 'view':
        {
          console.log('view');
        }
        break;

      default:
        break;
    }
  }

  openFilter(): void {
    this.draweService.create({
      nzContent: FiltersComponent,
      nzWidth: 450,
      nzClosable: false,
      nzTitle: null,
      nzContentParams: {
        toDisplayContent: 'tableFilters',
        categoryList: this.category,
      },
    });
  }

  getTableTitle(): string {
    let title = '';
    switch (this.className) {
      case 'received-orders':
        title = 'Received Orders';
        break;

      default:
        title = '';
        break;
    }
    return title;
  }

  isEnableFilterIcon(): boolean {
    this.isFilterEnable = false;
    switch (this.className) {
      case 'received-orders':
        this.isFilterEnable = true;
        break;
      default:
        this.isFilterEnable = false;
        break;
    }
    return this.isFilterEnable;
  }
  isEnableAddIcon(): boolean {
    let isaddEnable = false;
    switch (this.className) {
      case '':
        isaddEnable = true;
        break;
      default:
        isaddEnable = false;
        break;
    }
    return isaddEnable;
  }
}
