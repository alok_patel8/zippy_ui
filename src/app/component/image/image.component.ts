import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SellerService } from 'src/app/core/seller.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'zip-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.less'],
})
export class ImageComponent implements OnInit {
  fb = new FormData();
  constructor(private seller: SellerService) {}

  ngOnInit(): void {}

  singleImage(event: any): void {
    // const image = event.target.files;
    let { files } = event.target;
    files = files[0];
    console.log('this is image', files);
    this.fb.append('myImage', files);
  }

  save(): void {
    //   console.log('fb', this.fb.getAll);
    //   this.seller.Image(this.fb).subscribe((res: any) => {
    //     console.log('this is res', res);
    //   });
  }
}
