import { Component, OnInit } from '@angular/core';
import io from 'socket.io-client';

const SOCKET_ENDPOINT = `localhost:8090`;

@Component({
  selector: 'zip-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.less'],
})
export class ChatComponent implements OnInit {
  socket: any;

  constructor() {}

  ngOnInit(): void {
    this.initSocketConnection();
  }

  initSocketConnection(): any {
    this.socket = io(SOCKET_ENDPOINT);
  }

  send(): void {
    this.socket.emit('eventName', { temp: 'aaaa' });
  }
}
