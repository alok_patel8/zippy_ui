import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FdHomeComponent } from './fd-home/fd-home.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { FdHeaderComponent } from './layouts/fd-header/fd-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { AntDesignModule } from './ant.design/ant.design.module';
import { RestaurantsComponent } from './component/restaurants/restaurants.component';
import { FiltersComponent } from './component/filters/filters.component';
import { AddBranchComponent } from './component/add-seller-account/add-seller-account.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AuthGuard } from './auth/authGuard/auth.guard';
import { TokenInterceptor } from './auth/interceptor/token.interceptor';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
// import { SidebarComponent } from './layouts/sidebar/sidebar.component';
import { LayoutsModule } from './layouts/layouts/layouts.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ImageComponent } from './component/image/image.component';
import { DynamicTableComponent } from './component/dynamic-table/dynamic-table.component';
import { ShopDetailsComponent } from './component/shop-details/shop-details.component';
import { MatIconModule } from '@angular/material/icon';
import { FdFooterComponent } from './layouts/fd-footer/fd-footer.component';
import { CustomActionsComponent } from './component/custom-actions/custom-actions.component';
import { AvProductComponent } from './component/shop-details/av-product/av-product.component';
import { CheckoutComponent } from './component/checkout/checkout.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { CartComponent } from './component/cart/cart.component';
import { ZipCommonModule } from './component/zip-common/zip-common.module';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { CardContainerComponent } from './component/shop-details/card-container/card-container.component';
import { PriceCardComponent } from './component/cart/price-card/price-card.component';
import { NgxStripeModule } from 'ngx-stripe';
import { environment } from 'src/environments/environment';
import { PaymentSuccessComponent } from './component/payment-success/payment-success.component';
import { ChatComponent } from './component/chat/chat.component';
import { CustomColumnComponent } from './component/dynamic-table/custom-column/custom-column.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    FdHomeComponent,
    SignInComponent,
    FdHeaderComponent,
    RestaurantsComponent,
    FiltersComponent,
    AddBranchComponent,
    ImageComponent,
    DynamicTableComponent,
    ShopDetailsComponent,
    FdFooterComponent,
    CustomActionsComponent,
    AvProductComponent,
    CheckoutComponent,
    ForgetPasswordComponent,
    CartComponent,
    ChangePasswordComponent,
    CardContainerComponent,
    PriceCardComponent,
    PaymentSuccessComponent,
    ChatComponent,
    CustomColumnComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AntDesignModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    NgxMatSelectSearchModule,
    LayoutsModule,
    Ng2SmartTableModule,
    MatIconModule,
    ZipCommonModule,
    NgxStripeModule.forRoot(environment?.stripe_PubKey),
  ],
  // exports: [DeliveryAddressFormComponent],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
