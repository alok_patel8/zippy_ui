import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzModalService } from 'ng-zorro-antd/modal';
import { SignInComponent } from '../auth/sign-in/sign-in.component';
import { FiltersComponent } from '../component/filters/filters.component';
import { CommonService } from '../core/common.service';

const googleMapsAPIKey = 'AIzaSyDuXJtQ3KVgK3dqxCYRRx1wCqCnaijT7QA';
declare let google: any;

@Component({
  selector: 'zip-fd-home',
  templateUrl: './fd-home.component.html',
  styleUrls: ['./fd-home.component.less'],
})
export class FdHomeComponent implements OnInit {
  map: any;
  tagStyle = false;
  headingTag = [
    'Unexpected guests',
    'Late night at office',
    'Movie marathon',
    'Game night',
    'Cooking gone wrong',
    'Hungry',
  ];
  // headingtag;
  // latitude = 27.1446;
  // longitude = 83.5622;
  // zoom = 8;
  // mapOptions: any = {
  //   center: {
  //     lat: this.latitude,
  //     lng: this.longitude,
  //   },
  //   zoom: this.zoom,
  //   minZoom: 3,
  //   zoomControl: false,
  //   mapTypeControl: false,
  //   fullscreenControl: false,
  //   streetViewControl: false,
  // };

  // apiOptions: any = {
  //   version: 'weekly',
  //   libraries: ['places'],
  // };

  // mapLoaderOptions: any = {
  //   apiKey: googleMapsAPIKey,
  //   divId: 'google_map',
  //   append: false,
  //   mapOptions: this.mapOptions,
  //   apiOptions: this.apiOptions,
  // };

  constructor(
    private drawerService: NzDrawerService,
    private modal: NzModalService,
    private commonService: CommonService,
    private router: Router
  ) {
    console.log('this is home module', localStorage.getItem('_crd'));
  }

  ngOnInit(): void {
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    console.log(
      'this is home module',
      localStorage.getItem('_crd'),
      user?.role
    );
    this.router.navigate([`/zippy/${user?.role ? user?.role : 'user'}`]);
  }

  openFilter(): void {
    this.modal.create({
      nzContent: FiltersComponent,
      nzFooter: null,
      nzTitle: 'Filter',
      nzMask: false,
    });
  }

  // tslint:disable-next-line: use-lifecycle-interface
  // ngAfterViewInit(): void {
  //   const mapLoader = new GoogleMap();
  //   mapLoader.initMap(this.mapLoaderOptions).then((googleMap: any) => {
  //     this.map = googleMap;
  //   });
  // }

  // getCurrentLocation(): void {
  //   const getLocation = document.getElementById('getLocation');
  //   getLocation.addEventListener('click', () => {
  //     if (navigator.geolocation) {
  //       navigator.geolocation.getCurrentPosition(
  //         (position: any) => {
  //           const pos = {
  //             lat: position.coords.latitude,
  //             lng: position.coords.longitude,
  //           };
  //           console.log('location', pos);
  //           this.map.setCenter(pos);
  //           const geocoder = new google.maps.Geocoder();
  //           this.geocodeLatLng(geocoder, this.map, pos);
  //         },
  //         (err) => {
  //           console.log(err);
  //         }
  //       );
  //     }
  //   });
  // }

  // geocodeLatLng(
  //   geocoder: google.maps.Geocoder,
  //   map: google.maps.Map,
  //   pos: any
  // ): void {
  //   const input = (document.getElementById('google_map') as HTMLInputElement)
  //     .value;
  //   const latlng = {
  //     lat: pos.lat,
  //     lng: pos.lng,
  //   };
  //   geocoder.geocode(
  //     { location: latlng },
  //     (
  //       results: google.maps.GeocoderResult[],
  //       status: google.maps.GeocoderStatus
  //     ) => {
  //       if (status === 'OK') {
  //         if (results[0]) {
  //           map.setZoom(11);
  //           const marker = new google.maps.Marker({
  //             position: latlng,
  //             map: this.map,
  //           });
  //           window.alert(results[0].formatted_address);
  //         } else {
  //           window.alert('No results found');
  //         }
  //       } else {
  //         window.alert('Geocoder failed due to: ' + status);
  //       }
  //     }
  //   );
  // }

  openDrawer(formType: any): void {
    this.drawerService.create({
      nzContent: SignInComponent,
      nzTitle: null,
      nzWidth: 450,
      nzClosable: false,
      nzBodyStyle: { padding: 0 },
      nzContentParams: {
        formType,
      },
    });
  }
}
