import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FdHomeRoutingModule } from './fd-home-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FdHomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class FdHomeModule {}
