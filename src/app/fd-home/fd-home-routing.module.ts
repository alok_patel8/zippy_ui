import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/authGuard/auth.guard';
import { AddBranchComponent } from '../component/add-seller-account/add-seller-account.component';
import { ChatComponent } from '../component/chat/chat.component';
import { ImageComponent } from '../component/image/image.component';
import { RestaurantsComponent } from '../component/restaurants/restaurants.component';
import { AdminModule } from '../panels/admin/admin.module';
import { FdHomeComponent } from './fd-home.component';

const routes: Routes = [
  {
    path: 'user',
    loadChildren: () =>
      import('./../panels/users/users.module').then((m) => m.UsersModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('../auth/authModule/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('../panels/admin/admin-routing.module').then((m) => AdminModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'seller',
    loadChildren: () =>
      import('./../panels/seller/seller.module').then((m) => m.SellerModule),
    canActivate: [AuthGuard],
  },

  {
    path: 'image',
    component: ImageComponent,
    pathMatch: 'full',
  },
  {
    path: 'addBranch',
    component: AddBranchComponent,
    pathMatch: 'full',
  },
  {
    path: 'chat',
    component: ChatComponent,
    pathMatch: 'full',
  },
  { path: 'home', component: FdHomeComponent, pathMatch: 'full' },

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FdHomeRoutingModule {}
