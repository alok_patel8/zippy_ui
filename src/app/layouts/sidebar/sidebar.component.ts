import { Component, OnInit } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { CommonService } from 'src/app/core/common.service';

@Component({
  selector: 'zip-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less'],
})
export class SidebarComponent implements OnInit {
  userRole: any;
  constructor(
    private commonService: CommonService,
    private defServiceRef: NzDrawerRef
  ) {}

  ngOnInit(): void {
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.userRole = user?.role;
  }

  closeDrawer(): void {
    this.defServiceRef.close();
  }
}
