import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FdHeaderComponent } from './fd-header.component';

describe('FdHeaderComponent', () => {
  let component: FdHeaderComponent;
  let fixture: ComponentFixture<FdHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FdHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FdHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
