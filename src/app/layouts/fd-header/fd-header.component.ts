import { flatten } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';
import { AuthService } from 'src/app/auth/authService/auth.service';
import { SignInComponent } from 'src/app/auth/sign-in/sign-in.component';
import { CartService } from 'src/app/core/cart.service';
import { CommonService } from 'src/app/core/common.service';
import { SidebarComponent } from '../sidebar/sidebar.component';

@Component({
  selector: 'zip-fd-header',
  templateUrl: './fd-header.component.html',
  styleUrls: ['./fd-header.component.less'],
})
export class FdHeaderComponent implements OnInit {
  appNav = true;
  isUserloggedIn = false;
  loader = false;
  userName: string;
  cartlength: any;
  userRole: any;
  branch: any;
  selectedBranch = new FormControl('');
  userId: any;
  cardItemList: string[];
  constructor(
    private drawerService: NzDrawerService,
    private commonService: CommonService,
    private authService: AuthService,
    private router: Router,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.isUserloggedIn = this.authService.isUserLoggedIn();
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    this.getCartItems();
    this.setUserData(user);
    this.commonService.loader.subscribe((res: any) => {
      this.loader = res;
    });
    this.cartService.userCart.subscribe((res: any) => {
      this.cartlength = res?.totalCartItem;
    });
    this.commonService.userLoggedStatus.subscribe((res: any) => {
      if (res) {
        this.setUserData(res?.user);
        this.getCartItems();
      }
    });
    this.CalculateCartQuantity();
  }

  setUserData(user: any): void {
    if (user !== undefined) {
      this.isUserloggedIn = true;
      this.userId = user?._id;
      this.userName = user?.fullname;
      this.userRole = user?.role;
      this.branch = user?.branch;
    } else {
      this.isUserloggedIn = false;
    }
    // this.cartlength = JSON.parse(localStorage.getItem('_card')).length;
  }

  onSelectBranch(): void {
    localStorage.setItem('_brh', this.selectedBranch.value);
  }

  logout(): void {
    const user = this.commonService.decryptData(localStorage.getItem('_crd'));
    if (user?.role === 'seller') {
      this.router.navigate(['/zippy']);
    } else if (user?.role === 'admin') {
      this.router.navigate(['/zippy']);
    }
    localStorage.removeItem('_token');
    localStorage.removeItem('_crd');
    this.commonService.userLoggedStatus.emit('logout');
    this.isUserloggedIn = false;
    this.CalculateCartQuantity();
    this.userName = '';
    this.userRole = '';
    this.branch = [];
  }

  openDrawer(): void {
    this.drawerService.create({
      nzContent: SignInComponent,
      nzTitle: null,
      nzWidth: 450,
      nzClosable: false,
      nzBodyStyle: { padding: 0 },
      nzContentParams: {
        formType: true,
      },
    });
  }

  getCartItems(): void {
    if (this.isUserloggedIn && this.userRole === 'user') {
      this.cartService.getCartData(this.userId).subscribe((cart: any) => {
        this.cartlength = cart?.totalCartItem;
        this.cartService.userCart.next(cart);
      });
    }
  }

  CalculateCartQuantity(data?: any): void {
    if (this.isUserloggedIn) {
      this.cartlength = data;
    } else {
      this.cartlength = 0;
      const cart = JSON.parse(localStorage.getItem('cart'));
      cart?.forEach((ele: any) => {
        this.cartlength = this.cartlength + ele.quantity;
      });
    }
  }
}

// let appNav = document.getElementById('appNav');
// window.addEventListener('scroll', () => {
//   const x = window.scrollY;
//   if (x > 200) {
//     // appNav.style.display = 'none';
//     this.appNav = false;
//   }
//   if (x < 200) {
//     // appNav.style.display = 'block';
//     this.appNav = true;
//   }
//   console.log(x);
// });
