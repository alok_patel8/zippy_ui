import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { AntDesignModule } from 'src/app/ant.design/ant.design.module';

@NgModule({
  declarations: [SidebarComponent],
  imports: [CommonModule, RouterModule, AntDesignModule],
  exports: [SidebarComponent],
})
export class LayoutsModule {}
