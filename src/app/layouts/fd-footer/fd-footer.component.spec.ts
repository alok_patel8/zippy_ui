import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FdFooterComponent } from './fd-footer.component';

describe('FdFooterComponent', () => {
  let component: FdFooterComponent;
  let fixture: ComponentFixture<FdFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FdFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FdFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
