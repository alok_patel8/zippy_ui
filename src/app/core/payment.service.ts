import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

function _window(): any {
  return window;
}

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  SERVER_URL = environment.devenvironmentUrl;
  get nativeWindow(): any {
    return _window();
  }
  constructor(private http: HttpClient) {}

  stripePayment(data: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/payment/stripe/payment', data);
  }

  createPeymentSession(data: any): Observable<any> {
    return this.http.post(
      this.SERVER_URL + '/payment/create-checkout-session',
      data
    );
  }
  RazorPayPeymentSession(data: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/payment/create-payment', data);
  }

  getPeymentSession(data: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/payment/getpaymentdetails', data);
  }

  getCartItems(id: any): Observable<any> {
    return this.http.get(this.SERVER_URL + '/payment/cartdata?id=' + id);
  }
}
