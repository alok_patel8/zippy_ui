import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  SERVER_URL = environment.devenvironmentUrl;

  constructor(private http: HttpClient) {}

  saveDeliverAddress(payload: object): Observable<any> {
    return this.http.post(this.SERVER_URL + '/users/save-address', payload);
  }

  updateDeliverAddress(id: string, payload: object): Observable<any> {
    return this.http.put(
      this.SERVER_URL + '/users/update-address?id=' + id,
      payload
    );
  }

  DeleteDeliverAddress(id: string): Observable<any> {
    return this.http.delete(this.SERVER_URL + '/users/update-address?id=' + id);
  }

  getDeliveryAddress(id: string): Observable<any> {
    return this.http.get(this.SERVER_URL + '/users/del-address?id=' + id);
  }

  getPinCodeDetails(pin: any): Observable<any> {
    return this.http.get(this.SERVER_URL + '/users/pincode?id=' + pin);
  }
}
