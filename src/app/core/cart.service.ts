import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/authService/auth.service';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  SERVER_URL = environment?.devenvironmentUrl;
  shopDetails: any;
  cardItemList = [];
  cartItems: any = JSON.parse(localStorage.getItem('cart')) || [];
  user = this.commonService.decryptData(localStorage.getItem('_crd'));

  updatecartItem = new EventEmitter();
  TotalCartItems = new EventEmitter();
  userCart = new BehaviorSubject({});
  constructor(
    private commonService: CommonService,
    private authService: AuthService,
    private http: HttpClient
  ) {}

  saveShopData(data: any): void {
    const shop = localStorage.getItem('_shop');
    if (!shop) {
      const cart = localStorage.setItem(
        '_shop',
        this.commonService.decryptData(data)
      );
    } else {
      this.shopDetails = this.commonService.encryptData(shop);
      // console.log('this is sss', this.shopDetails);
    }
  }

  removeShop(): void {
    localStorage.removeItem('_shop');
  }

  addNewDataInCart(itemId: any): any {
    // if (this.authService.isUserLoggedIn()) {
    //   return this.UpdateCardData('New', itemId).subscribe((res) => {
    //     this.ItemsInCart(res?.data);
    //     this.TotalCartItems.emit(res);
    //   });
    // } else {
    const cart = this.cartItems;
    if (cart === null) {
      cart.push({
        itemId,
        quantity: 1,
      });
    } else {
      const find = cart?.filter((i: any) => i?.itemId === itemId);
      if (find.length === 0) {
        cart.push({
          itemId,
          quantity: 1,
        });
      }
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    this.cardItemList.push(itemId);
    // this.ItemsInCart();
    // }
  }

  updateQuantity(action: string, itemId: string): void {
    // if (this.authService.isUserLoggedIn()) {
    //   this.UpdateCardData(action, itemId).subscribe((res) => {
    //     this.ItemsInCart(res?.data);
    //     this.TotalCartItems.emit(res);
    //   });
    // } else {
    this.cartItems = JSON.parse(localStorage.getItem('cart'));
    this.cartItems?.forEach((e: any) => {
      if (itemId === e?.itemId) {
        switch (action) {
          case 'add':
            {
              if (e?.quantity < 10) {
                e.quantity += 1;
              }
            }
            break;
          case 'sub':
            {
              if (e?.quantity > 0) {
                e.quantity -= 1;
                if (e?.quantity === 0) {
                  this.cartItems?.splice(this.cartItems?.indexOf(e), 1);
                  this.cardItemList.splice(
                    this.cardItemList.indexOf(itemId),
                    1
                  );
                }
              }
            }
            break;
          default:
            break;
        }
      }
    });
    localStorage.setItem('cart', JSON.stringify(this.cartItems));
    // this.ItemsInCart();
    // }
  }

  UpdateCardData(
    action: string,
    itemId: string,
    shopId: string
  ): Observable<any> {
    return this.http.post(this.SERVER_URL + '/users/cart', {
      itemId,
      userid: this.user?._id,
      shopId,
      action,
    });
  }

  getCartData(id: any): Observable<any> {
    return this.http.get(this.SERVER_URL + '/users/getcart?id=' + id);
  }

  removeAllCartItems(paylod: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/users/remove-cart-items', paylod);
  }

  saveOrders(payload: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/users/save/orders', payload);
  }

  ItemsInCart(data: any, action: string): Array<string> {
    const child = [];
    const parent = [];
    if (this.authService.isUserLoggedIn()) {
      data?.forEach((e: any) => {
        child.push(e?.l?.q?._id);
        parent.push(e?.parentId);
      });
      return action === 'child' ? child : parent;
    } else {
      return [];
    }
  }
}
