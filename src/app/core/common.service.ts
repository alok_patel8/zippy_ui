import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as cryptoJS from 'crypto-js';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  SERVER_URL = environment?.devenvironmentUrl;
  userLoggedStatus = new EventEmitter();
  deleteEvent = new EventEmitter();
  ItemAddEvent = new EventEmitter();
  OrederList = new EventEmitter();
  loader = new EventEmitter();
  changeCardData = new EventEmitter();
  ShopData: any = new BehaviorSubject({});
  rowdata: any = new BehaviorSubject({});

  userdata = new BehaviorSubject({});

  constructor(private message: MatSnackBar, private http: HttpClient) {}

  showSnackBar(message: string): any {
    this.message.open(
      message.replace(/\b\w/g, (first) => first.toLocaleUpperCase()),
      '',
      {
        duration: 4000,
        horizontalPosition: 'start',
        verticalPosition: 'bottom',
      }
    );
  }

  encryptData(data): any {
    try {
      return cryptoJS.AES.encrypt(
        JSON.stringify(data),
        'encryptSecretKey'
      ).toString();
    } catch (e) {
      console.log(e);
    }
  }

  decryptData(data): any {
    try {
      const bytes = cryptoJS.AES.decrypt(data, 'encryptSecretKey');
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(cryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }

  deleteItemsById(payload: any): Observable<any> {
    return this.http.post(`${environment?.devenvironmentUrl}/delete`, payload);
  }

  getDetailsById(payload: any): Observable<any> {
    return this.http.post(`${environment?.devenvironmentUrl}/details`, payload);
  }
  getCardData(): Observable<any> {
    return this.http.get(`${environment?.devenvironmentUrl}/shop`);
  }

  getShopDataById(payload: any): Observable<any> {
    return this.http.get(`${environment?.devenvironmentUrl}/getShopdata`, {
      params: { payload },
    });
  }
  getSellerShopDataById(payload: any): Observable<any> {
    return this.http.get(
      `${environment?.devenvironmentUrl}/seller/getShopdata`,
      {
        params: { payload },
      }
    );
  }

  getOrders(idType: any, id: any): Observable<any> {
    return this.http.get(
      this.SERVER_URL + '/seller/getorders?' + idType + '=' + id
    );
  }
}
