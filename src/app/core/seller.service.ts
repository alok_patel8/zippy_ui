import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SellerService {
  SERVER_URL = environment.devenvironmentUrl;
  constructor(private http: HttpClient) {}

  registration(payload): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/seller/account`,
      payload
    );
  }
  addProduct(payload: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/seller/addproduct`,
      payload
    );
  }
  addFirstStepData(payload: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/seller/addproduct`,
      payload
    );
  }

  addProductQuant(payload: any): Observable<any> {
    return this.http.post(this.SERVER_URL + '/seller/addquant', payload);
  }

  uploadProductImage(id: any, payload: any): Observable<any> {
    return this.http.post(
      this.SERVER_URL + '/seller/uploadimage?id=' + id,
      payload
    );
  }
  getProductImage(): Observable<any> {
    return this.http.get(this.SERVER_URL + '/seller/getImagelist');
  }

  addQuantity(id: any, payload: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/seller/addQuantity`,
      { payload, id }
    );
  }

  getBranchProduct(payload: any): Observable<any> {
    return this.http.get(`${environment.devenvironmentUrl}/seller/products`, {
      params: { payload },
    });
  }
  getProductById(payload: any): Observable<any> {
    return this.http.get(
      `${environment.devenvironmentUrl}/seller/productsbyid`,
      {
        params: { payload },
      }
    );
  }

  GetZippyImage(): any {
    return this.http.get(
      `${environment.devenvironmentUrl}/seller/getAllproduct`
    );
  }

  SellerDashboard(): any {
    return this.http.get(this.SERVER_URL + `/seller/sellerdashboard`);
  }

  updateStoreItem(itemId: any, data: any): Observable<any> {
    return this.http.put(this.SERVER_URL + '/seller/updateItem', {
      itemId,
      data,
    });
  }
}
