import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AdminService {
  SERVER_URL = environment.devenvironmentUrl;
  constructor(private http: HttpClient) {}

  saveCategory(payload: Array<Object>): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/admin/addcategory`,
      { payload }
    );
  }

  getCategory(): Observable<any> {
    return this.http.get(`${environment.devenvironmentUrl}/admin/category`);
  }
  updateCategory(payload: any): Observable<any> {
    return this.http.post(
      `${environment.devenvironmentUrl}/admin/updatecategory`,
      payload
    );
  }

  deleteCategory(payload: any): Observable<any> {
    return this.http.post(`${environment.devenvironmentUrl}/delete`, payload);
  }

  AdminDashboard(): Observable<any> {
    return this.http.get(this.SERVER_URL + '/admin/admindashboard');
  }
}
