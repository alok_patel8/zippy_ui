import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'zippy',
    loadChildren: () =>
      import('./fd-home/fd-home.module').then((m) => m.FdHomeModule),
  },
  {
    path: '',
    redirectTo: 'zippy',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
